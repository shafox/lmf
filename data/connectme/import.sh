#!/bin/bash

### Sample File ###
curl -i -H "Content-Type: application/rdf+xml" -X POST -d @data/sample.xml http://localhost:8080/KiWi2/import/upload
curl -L -i -H "Content-Type: video/ogg; rel=content" -X PUT --data-binary @data/sample.ogv http://localhost:8080/KiWi2/resource/sample.ogv

