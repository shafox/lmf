#!/bin/bash
curl -i -H "Content-Type: text/plain" -X POST --data-binary @sag.kwrl  http://localhost:8080/LMF/reasoner/program/sag
curl -i -H "Content-Type: application/rdf+xml" -X POST -d @foaf.owl http://localhost:8080/LMF/import/upload
curl -i -H "Content-Type: application/rdf+xml" -X POST -d @doctype-ontology.rdf http://localhost:8080/LMF/import/upload
curl -i -H "Content-Type: application/rdf+xml" -X POST -d @project-ontology.rdf http://localhost:8080/LMF/import/upload
curl -i -H "Content-Type: application/rdf+xml" -X POST -d @srfg-ontology.rdf http://localhost:8080/LMF/import/upload
