/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.templating.writer;

import freemarker.template.Configuration;
import freemarker.template.Template;
import kiwi.core.api.io.RDFWriterPriority;
import kiwi.core.rio.RDFHtmlWriterImpl;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;

/**
 * RDF to HTML Extended Writer
 * 
 * @author Thomas Kurz
 * @author Sergio Fernández
 */
public class RDFHtmlExtendedWriter extends RDFHtmlWriterImpl {

	private Template template;

    public RDFHtmlExtendedWriter(OutputStream out) {
        super(out);
        initTemplate();
    }

    public RDFHtmlExtendedWriter(Writer writer) {
        this(new PrintWriter(writer));
    }

    public RDFHtmlExtendedWriter(PrintWriter writer) {
        super(writer);
        initTemplate();
    }

	//TODO init template does not work !!!
	// -> The whole writer does not work
	private void initTemplate() {
		Configuration cfg = new Configuration();
        try {
            cfg.setClassForTemplateLoading(this.getClass(),"/templates/");
            template = cfg.getTemplate(TEMPLATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	@Override
	public RDFWriterPriority getPriority() {
		return RDFWriterPriority.HIGH;
	}
	
}
