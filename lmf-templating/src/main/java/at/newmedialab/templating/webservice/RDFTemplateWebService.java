/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.templating.webservice;

import at.newmedialab.templating.service.RDFTemplateService;
import kiwi.core.api.content.ContentService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URLDecoder;

/**
 * User: Thomas Kurz
 * Date: 30.07.12
 * Time: 13:00
 */
@ApplicationScoped
@Path("/template")
public class RDFTemplateWebService {

	@Inject
	RDFTemplateService templateService;

	@Inject
	ContentService contentService;

	@GET
	@Produces("application/json")
	public Response getTemplates() {
		try {
			return Response.ok().entity(templateService.listTemplates()).build();
		} catch (Exception e) {
			return Response.serverError().entity(e.getMessage()).build();
		}
	}

	@POST
	@Produces("application/json")
	public Response addTemplate1(@QueryParam("title")String title, @QueryParam("template")String template, @Context HttpServletRequest request) {
		try {
			if(template==null) {
				return addTempl(request.getInputStream(),title);
			} else {
				return addTempl(new ByteArrayInputStream(template.getBytes("UTF-8")),title);
			}
		} catch(Exception e) {
			return Response.serverError().entity(e.getMessage()).build();
		}
	}

	private Response addTempl(InputStream input, String title) {
		try {
			title = title!=null ? URLDecoder.decode(title,"UTF-8") : null;
			return Response.ok(templateService.addTemplate(title,input)).build();
		} catch (Exception e) {
			return  Response.serverError().entity(e.getMessage()).build();
		}
	}

	@DELETE
	@Produces("application/json")
	public Response removeTemplate(@QueryParam("template")String template) {
		if(template==null) return Response.status(Response.Status.BAD_REQUEST).entity("query parameter template must be set").build();
		try {
			templateService.deleteTemplate(URLDecoder.decode(template,"UTF-8"));
			return Response.ok().build();
		} catch (Exception e) {
			return  Response.serverError().entity(e.getMessage()).build();
		}
	}

}
