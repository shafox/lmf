/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.templating.webservice;

import at.newmedialab.templating.service.RDFTemplatingService;
import kiwi.core.api.triplestore.SesameService;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;

/**
 * User: Thomas Kurz
 * Date: 31.07.12
 * Time: 09:12
 */
@ApplicationScoped
@Path("/templating")
public class RDFTemplatingWebservice {

    @Inject
    RDFTemplatingService templatingService;

    @Inject
    SesameService sesameService;

    /**
     * template resource with given template and return the result
     *
     * @param resource
     * @param template
     * @return response
     */
    @GET
    @Path("/run")
    @Produces("text/html")
    public Response template(@QueryParam("resource")final String resource,@QueryParam("template")final String template) {
        if(resource==null||template==null) return Response.status(Response.Status.BAD_REQUEST).entity("query parameter resource and template must be set").build();

        try {
            StreamingOutput entity = new StreamingOutput() {
                @Override
                public void write(OutputStream output) throws IOException, WebApplicationException {
                    try {
                        RepositoryConnection conn = sesameService.getConnection();

                        try {
                            templatingService.template(
                                    conn.getValueFactory().createURI(resource),
                                    conn.getValueFactory().createURI(template),
                                    output);
                            conn.commit();
                        } catch(IOException e) {
                            conn.rollback();
                            throw e;
                        }  catch (WebApplicationException e) {
                            conn.rollback();
                            throw e;
                        } finally {
                            conn.close();
                        }
                    } catch (RepositoryException e) {
                        throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
                    }
                }
            };
            return Response.ok().entity(entity).build();
        } catch(Exception e) {
            return  Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * return all templates for a given resource
     *
     * @param resource
     * @return response
     */
    @GET
    @Produces("application/json")
    public Response getTemplates(@QueryParam("resource")String resource) {
        if(resource==null) return Response.status(Response.Status.BAD_REQUEST).entity("query parameter resource must be set").build();

        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {
                return Response.ok().entity(templatingService.getTemplates(conn.getValueFactory().createURI(resource))).build();
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (Exception e) {
            return  Response.serverError().entity(e.getMessage()).build();
        }
    }


    /**
     * adds a template to a resource
     *
     * @param resource
     * @param template
     * @return response
     */
    @POST
    @Produces("application/json")
    public Response addTemplate(@QueryParam("resource")String resource,@QueryParam("template")String template) {
        if(resource==null||template==null) return Response.status(Response.Status.BAD_REQUEST).entity("query parameter resource and template must be set").build();
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {
                templatingService.addTemplate(
                        conn.getValueFactory().createURI(resource),
                        conn.getValueFactory().createURI(template));
                return Response.ok().build();
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (Exception e) {
            return  Response.serverError().entity(e.getMessage()).build();
        }
    }

    /**
     * removes a template from a resource
     *
     * @param resource
     * @param template
     * @return response
     */
    @DELETE
    @Produces("application/json")
    public Response removeTemplate(@QueryParam("resource")String resource,@QueryParam("template")String template) {
        if(resource==null||template==null) return Response.status(Response.Status.BAD_REQUEST).entity("query parameter resource and template must be set").build();
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {
                templatingService.removeTemplate(
                        conn.getValueFactory().createURI(resource),
                        conn.getValueFactory().createURI(template));
                return Response.ok().build();
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (Exception e) {
            return  Response.serverError().entity(e.getMessage()).build();
        }
    }

}
