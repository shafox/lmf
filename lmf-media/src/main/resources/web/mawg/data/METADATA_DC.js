/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var DATA_META = [
    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d1b-identifier.xml",properties:"identifier",result:"data/Db-identifier_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d2b-title.xml",properties:"title",result:"data/Db-title_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d3b-language.xml",properties:"language",result:"data/Db-language_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d4b-locator.xml",properties:"locator",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d5b-contributor.xml",properties:"contributor",result:"data/Db-contributor_ma_dc.json",remark:"testdata is wrong ('contributEr')"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d6b-creator.xml",properties:"creator",result:"data/Db-creator_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d7b-date.xml",properties:"date",result:"data/Db-date_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d8b-location.xml",properties:"location",result:"data/Db-location_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d9b-description.xml",properties:"description",result:"data/Db-description_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d10b-keyword.xml",properties:"keyword",result:"data/Db-keyword_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d11b-genre.xml",properties:"genre",result:"data/da-genre_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d12b-rating.xml",properties:"rating",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d13b-relation.xml",properties:"relation",result:"data/Db-relation_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d14b-collection.xml",properties:"collection",result:"data/da-collection_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d15b-copyright.xml",properties:"copyright",result:"data/Db-copyright_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d16b-policy.xml",properties:"policy",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d17b-publisher.xml",properties:"publisher",result:"data/Db-publisher_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d18b-targetAudience.xml",properties:"targetAudience",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d19b-fragment.xml",properties:"fragment",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d20b-namedFragment.xml",properties:"namedFragment",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d21b-frameSize.xml",properties:"frameSize",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d22b-compression.xml",properties:"compression",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d23b-duration.xml",properties:"duration",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d24b-format.xml",properties:"format",result:"data/Db-format_ma_dc.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d25b-samplingRate.xml",properties:"samplingRate",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d26b-frameRate.xml",properties:"frameRate",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d27b-averageBitRate.xml",properties:"averageBitRate",result:"data/no_DC_mapping.json"},

    {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d28b-numTracks.xml",properties:"numTracks",result:"data/no_DC_mapping.json"}
];

var DATA_ORIG = {uri:"http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/d29-getOriginalMetadata.xml",result:"data/getOriginalMetadata-DC.json"};