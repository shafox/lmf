/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function MetadataWriter(object) {

    this.endpoint = "http://localhost:8080/KiWi2/sparql/select";
    this.type = "RESOURCE";
    //initializer
    this.init = function(object) {
       if(object.target)this.target = object.target; else console.log("no 'target' defined for MetadataWriter");
       if(object.resource)this.resource = object.resource; else console.log("no 'resource' defined for MetadataWriter");
       if(object.endpoint)this.endpoint = object.endpoint; else console.log("using default endpoint for MetadataWriter");
       //delete hash
       if(this.resource.indexOf("#")>0) {
           this.resource = this.resource.substring(0,this.resource.indexOf("#"));
           this.type="FRAGMENT";
       }
       this.getValues();
    }

    this.getValues = function() {
        var self = this;
        var sparql =    "SELECT ?title ?identifier ?location ?format ?creator ?duration WHERE {" +
                        "   <"+this.resource+"> <http://www.w3.org/ns/ma-ont#title> ?title." +
                        "   <"+this.resource+"> <http://www.w3.org/ns/ma-ont#identifier> ?identifier." +
                        "   <"+this.resource+"> <http://www.w3.org/ns/ma-ont#hasFormat> ?format." +
                        "   <"+this.resource+"> <http://www.w3.org/ns/ma-ont#hasRelatedLocation> ?x1." +
                        "   ?x1 <http://www.w3.org/2000/01/rdf-schema#label> ?location." +
                        "   <"+this.resource+"> <http://www.w3.org/ns/ma-ont#hasCreator> ?x2." +
                        "   ?x2 <http://xmlns.com/foaf/0.1/name> ?creator." +
                        "   <"+this.resource+"> <http://www.w3.org/ns/ma-ont#duration> ?duration." +
                        "}";
        $.getJSON(this.endpoint+"?query="+encodeURIComponent(sparql)+"&output=json",function(data){
            self.writeValues(data);
        });
    }

    this.writeValues = function(data) {
        //if there are metadata
        if(data.results.bindings.length>0) {
            var o = data.results.bindings[0];

            //write table
            var table = $("<table></table>");
            table.append("<tr><td style='width:100px;vertical-align:top;'><b>"+this.type+"</b></td><td></td></tr>");
            table.append("<tr><td style='width:100px;vertical-align:top;'><b>Title:</b></td><td>"+o.title.value+"</td></tr>");
            table.append("<tr><td style='width:100px;vertical-align:top;'><b>Identifier:</b></td><td>"+o.identifier.value+"</td></tr>");
            table.append("<tr><td style='width:100px;vertical-align:top;'><b>Format:</b></td><td>"+o.format.value+"</td></tr>");
            table.append("<tr><td style='width:100px;vertical-align:top;'><b>Duration:</b></td><td>"+o.duration.value+"</td></tr>");
            table.append("<tr><td style='width:100px;vertical-align:top;'><b>Creator:</b></td><td>"+o.creator.value+"</td></tr>");
            table.append("<tr><td style='width:100px;vertical-align:top;'><b>Location:</b></td><td>"+o.location.value+".</td></tr>");
            //append table
            this.target.append(table);
        } else {
            this.target.html("<b>No Metadata for "+this.resource+"</b>");
        }
    }

    this.listener = function(object) {
        //nothing to do
    }

    this.init(object);
}