/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.w3c.mawg.model.xpath;

import nu.xom.Document;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

/**
 * User: Thomas Kurz
 * Date: 09.03.12
 * Time: 15:33
 */
public interface XPathMapper {
	public void toTriples(URI resource, Document document,  RepositoryConnection connection, ValueFactory factory, ConfigurationService configurationService) throws RepositoryException;
}
