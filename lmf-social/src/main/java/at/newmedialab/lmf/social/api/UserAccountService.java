/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.api;

import java.util.List;
import java.util.Map;

import at.newmedialab.lmf.social.model.UserAccount;

/**
 * User account service definition 
 * 
 * @author Sergio Fernández
 *
 */
public interface UserAccountService {
	
	UserAccount save(UserAccount account);
	
	UserAccount save(UserAccount account, String context);
	
	UserAccount save(String person, UserAccount account);
	
	UserAccount save(String person, UserAccount account, String context);
	
	boolean exists(String person);

	List<UserAccount> getUserAccounts(String person);
	
	Map<String, Double> getPreferences(String person);

}
