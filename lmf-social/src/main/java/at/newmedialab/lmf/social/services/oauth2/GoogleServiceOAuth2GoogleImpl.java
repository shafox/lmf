/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.services.oauth2;

import java.io.IOException;

import javax.enterprise.inject.Alternative;
import javax.validation.constraints.NotNull;

import at.newmedialab.lmf.social.api.google.GoogleService;
import at.newmedialab.lmf.social.model.UserAccount;
import at.newmedialab.lmf.social.model.google.GoogleUserProfile;

import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfo;

/**
 * Google service OAuth2 implemented using Google libraries
 * (not properly working)
 * 
 * @author Sergio Fernández
 *
 */
@Deprecated
@Alternative
public class GoogleServiceOAuth2GoogleImpl implements GoogleService {
    
    @Override
    public GoogleUserProfile getUserProfile(@NotNull String accessToken) {
        Oauth2 oauth2 = buildOAuth2(accessToken);
        Userinfo userInfo = null;
        try {
            userInfo = oauth2.userinfo().get().execute(); //FIXME: 401 on execute()
        } catch (IOException e) {
            throw new RuntimeException("Error obtaining user info from Google", e);
        }
        if (userInfo != null && userInfo.getId() != null) { 
            return new GoogleUserProfile(userInfo);
        } else {
            throw new UnsupportedOperationException("no user info found");
        }
    }

    private synchronized Oauth2 buildOAuth2(String accessToken) {
        HttpTransport httpTransport = new NetHttpTransport();
        JsonFactory jsonFactory = new JacksonFactory();
        Credential credentials = new Credential(BearerToken.authorizationHeaderAccessMethod());
        credentials.setAccessToken(accessToken);
        return new Oauth2.Builder(httpTransport, jsonFactory, credentials.getRequestInitializer())
                .setApplicationName(APPLICATION_NAME).build();
    }

	@Override
	public void retrieveContent(UserAccount account, String context, String accessToken) {
		// TODO Auto-generated method stub
		
	}  

}
