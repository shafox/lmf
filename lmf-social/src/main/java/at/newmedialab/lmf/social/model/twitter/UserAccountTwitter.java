/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.model.twitter;

import at.newmedialab.lmf.social.model.UserAccount;

/**
 * User account at Twitter
 * 
 *
 */
public class UserAccountTwitter extends UserAccount {

	private static final String SERVICE = "http://twitter.com";

	public UserAccountTwitter() {
		super();
		setService(SERVICE);
	}

	public UserAccountTwitter(String id, String username, String name,
			String firstname, String familyname, String gender,
			String birthday, String email, String address, String location,
			String affiliation, String homepage, String picture) {
		super(id, username, name, firstname, familyname, gender, birthday, email,
				address, location, affiliation, homepage, picture);
		setService(SERVICE);
	}

	public UserAccountTwitter(TwitterUserProfile profile) {
		// TODO Auto-generated constructor stub
	}

}
