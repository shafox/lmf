/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.model.rss;

import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;
import org.apache.marmotta.commons.sesame.model.Namespaces;

import java.util.Date;
import java.util.LinkedList;

import static at.newmedialab.lmf.social.model.Constants.NS_FCP_RSS;
import static org.apache.marmotta.commons.sesame.model.Namespaces.NS_DC;
import static org.apache.marmotta.commons.sesame.model.Namespaces.NS_RSS;

@RDFType(NS_RSS + "channel")
public interface RSSFeedFacade extends Facade {

    @RDF(NS_RSS + "title")
    String getTitle();
    void setTitle(String title);

    @RDF(NS_RSS + "description")
    String getDescription();
    void setDescription(String description);

    @RDF(NS_RSS + "link")
    String getLink();
    void setLink(String url);

    @RDF(NS_RSS + "items")
    LinkedList<RSSEntryFacade> getItems();
    void setItems(LinkedList<RSSEntryFacade> items);

    @RDF(NS_DC + "language")
    String getLanguage();
    void setLanguage(String lang);

    @RDF(NS_RSS + "url")
    String getImageUrl();
    void setImageUrl(String image);

    @RDF(NS_FCP_RSS + "feedSource")
    String getFeedSource();
    void setFeedSource(String srcURL);

    @RDF(NS_FCP_RSS + "lastUpdate")
    Date getUpdated();
    void setUpdated(Date date);

    @RDF(NS_FCP_RSS + "categories")
    LinkedList<RSSCategoryFacade> getCategories();
    void setCategories(LinkedList<RSSCategoryFacade> categories);

    @RDF(NS_FCP_RSS + "updateMechanism")
    RSSUpdateMechanism getUpdateMechanism();
    void setUpdateMechanism(RSSUpdateMechanism updateMechanism);
}
