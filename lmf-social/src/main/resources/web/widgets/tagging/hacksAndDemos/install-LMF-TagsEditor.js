/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * This script is used together with the Greasemonkey user script LMF-TagsEditor.user.js.
 *
 * It adds the LMF-TagsEditor to the current page. The reason why this is not
 * done directly in the user script is that there is an issue with
 * event handlers set in the user scripts.
 *
 */
(function(){
	var baseIFrameURL = "http://localhost:8080/KiWi2/social/widgets/tagging/iframeIntegration/panel.html";

	// Create an IFrame, load the simple HTML embedding the editor in it
	var iframe = document.createElement("IFRAME");
	iframe.id = "TagsEditorIFrame";
	iframe.src = baseIFrameURL + "#" + document.location;
	iframe.style.width = "300px";
	iframe.style.height = "30px";
	iframe.style.position = "fixed";
	iframe.style.right = "27px";
	iframe.style.bottom = "0px";
	document.body.appendChild(iframe);

	// Also create a simple "expand button"
	var expButton = document.createElement("DIV");
	expButton.id = "div";
	expButton.style.backgroundImage = "url('http://localhost:8080/KiWi2/social/widgets/tagging/img/lmf_icon_20x20px.png')"
	expButton.style.backgroundPosition = "center";
	expButton.style.backgroundRepeat = "no-repeat";
	expButton.style.cursor = "pointer";
	expButton.style.border = "2px solid gray";
	expButton.style.width = "27px";
	expButton.style.height = "30px";
	expButton.style.position = "fixed";
	expButton.style.right = "0px";
	expButton.style.bottom = "0px";
	expButton.innerHTML = "";

	// Expands the IFrame to a larger size
	function expand(){
		expButton.style.display = "none";
		iframe.style.height = "300px";
		iframe.style.right = "0px";

		// Pass "expanded" state into the IFrame via location hash
		iframe.src = baseIFrameURL + "#expanded";
	}
	expButton.onclick = expand;

	// Collapses the IFrame
	function collapse(){
		expButton.style.display = "block";
		iframe.style.height = "30px";
		iframe.style.right = "27px";

		window.location.hash = "";
	}

	document.body.appendChild(expButton);

	// Start checking for "collapsed" location hash on this outter frame;
	// the TagEditor running in the IFrame may set this hash on this frame
	// when it wants to be collapsed
	window.setInterval(function(){
		if (window.location.hash == "#collapsed") {
			collapse();
		}
	}, 100);
})();