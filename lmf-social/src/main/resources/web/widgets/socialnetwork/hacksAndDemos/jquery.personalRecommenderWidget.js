/*
 * Copyright (c) 2012 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function( $ ){
    $.fn.personal_recommender = function(options) {

        var viewAccount = new Array();

        var settings = {
            host: 'http://labs.newmedialab.at/MyTV/',
            path2json : 'meta/application/json',
            requested_resource: 'http://labs.newmedialab.at/MyTV/user/100003127765979'
        }

        var request_data = {
                nick: 'http://xmlns.com/foaf/0.1/nick',
                name: 'http://xmlns.com/foaf/0.1/name',
                firstName: "http://xmlns.com/foaf/0.1/firstName",
                lastName: "http://xmlns.com/foaf/0.1/lastName",
                familyName: "http://xmlns.com/foaf/0.1/familyName",
                age: "http://xmlns.com/foaf/0.1/age",
                gender: "http://xmlns.com/foaf/0.1/gender",
                birthday: "http://xmlns.com/foaf/0.1/birthday",
                onlineAccount: "http://xmlns.com/foaf/0.1/account",
                homepage: "http://xmlns.com/foaf/0.1/accountServiceHomepage",
                accountName: "http://xmlns.com/foaf/0.1/accountName"
        }

        var request = {

            getResource:function(resourceURI, callback) {

                $.ajax({
                    type:"GET",
                    contentType: "application/json",
                    url: settings.host+settings.path2json,
                    data: "uri="+resourceURI,
                    async: false,
                    success: function(result) {

                        var resource = new Array();
                        var meta = result[resourceURI];

                        try {
                            resource["nick"] = meta[request_data.nick][0]['value'];
                        } catch(e) {}
                        try {
                            resource["name"] = meta[request_data.name][0]['value'];
                        } catch(e) {}
                        try {
                            resource["firstName"] = meta[request_data.firstName][0]['value'];
                        } catch(e) {}
                        try {
                            resource["lastName"] = meta[request_data.lastName][0]['value'];
                        } catch(e) {}
                        try {
                            resource["familyName"] = meta[request_data.familyName][0]['value'];
                        } catch(e) {}
                        try {
                            resource["age"] = meta[request_data.age][0]['value'];
                        } catch(e) {}
                        try {
                            resource["gender"] = meta[request_data.gender][0]['value'];
                        } catch(e) {}
                        try {
                            resource["birthday"] = meta[request_data.birthday][0]['value'];
                        } catch(e) {}
                        try {
                            resource["homepage"] = meta[request_data.birthday][0]['value'];
                        } catch(e) {}
                        try {
                            resource["mbox"] = meta[request_data.birthday][0]['value'];
                        } catch(e) {}
                        try {
                            resource["onlineAccounts"] = new Array();
                            for(i=0; i<meta[request_data.onlineAccount].length; i++) {
                                resource["onlineAccounts"][i] = meta[request_data.onlineAccount][i]['value'];
                            }
                        } catch(e) {}

                        callback(resource);

                    },
                    error: function(jXHR) {
                        //should not happen
                        alert(jXHR.statusText);
                    }
                });
            },

            buildContent:function(res) {

                var contentDiv = $("<div id='personalRecommenderContent'></div>");

                var contentKeyDiv = $("<div id='personalRecommenderContentKey'></div>");
                var contentValueDiv = $("<div id='personalRecommenderContentValue'></div>");

                if(res["nick"]) {
                    contentKeyDiv.append("<li>Nick:</li>");
                    contentValueDiv.append("<li>"+res["nick"]+"</li>");
                }
                if(res["name"]) {
                    contentKeyDiv.append("<li>Name:</li>");
                    contentValueDiv.append("<li>"+res["name"]+"</li>");
                }
                if(res["age"]) {
                    contentKeyDiv.append("<li>Age:</li>");
                    contentValueDiv.append("<li>"+res["age"]+"</li>");
                }
                if(res["birthday"]) {
                    contentKeyDiv.append("<li>Birthday:</li>");
                    contentValueDiv.append("<li>"+res["birthday"]+"</li>");
                }
                if(res["onlineAccounts"]) {
                    contentKeyDiv.append("<li>OnlineAccounts:</li>");

                    for(i=0; i<res["onlineAccounts"].length; i++) {
                        contentValueDiv.append("<li>" + res["onlineAccounts"][i] +"</li>");
                        if(i != res["onlineAccounts"].length-1) {
                            contentKeyDiv.append("<li></li>");
                        }
                    }

                }
                if(res["interests"]) {
                    contentKeyDiv.append("<li>Interests:</li>");
                    li = $("<li>");
                    for(i=0; i<res["interests"].length; i++) {
                        if(i != 0) {
                            li.append(", ");
                        }
                        li.append(res["interests"][i] + " ");
                    }
                    contentValueDiv.append(li);
                }

                contentDiv.append(contentKeyDiv);
                contentDiv.append(contentValueDiv);

                return contentDiv;
            }
        }

        /**
         * main method
         */
        return this.each(function() {

            var self = $(this);
            var res = new Array();
            
            res = request.getResource(settings.requested_resource, function(res) {

                var counter = 0;
                var headerDiv = $("<div id='personalRecommenderHeader'></div>");
                headerDiv.append("Welcome ");
                if(res["name"]) {
                    headerDiv.append(res["name"]);
                }
                else if(res["firstName"] && res["lastName"]) {
                    headerDiv.append(res["firstName"] + res["lastName"]);
                }
                else if(res["firstName"]) {
                    headerDiv.append(res["firstName"]);
                }
                else if(res["nick"]) {
                    headerDiv.append(res["nick"]);
                }

                var navigationDiv = $("<div id='personalRecommenderNav'></div>");
                navigationDiv.append("<li>LMF Data</li>");

                if(res["onlineAccounts"]) {
                    for(i=0; i<res["onlineAccounts"].length; i++) {
                        var acc = request.getResource(res["onlineAccounts"][i], function() {});
                        if(acc["homepage"]) {
                            alert(acc["homepage"]);
                            if(acc["homepage"] == "https://www.facebook.com/") {
                                navigationDiv.append("<li><a onclick='request.buildContent(settings.requested_resource);'>Facebook Data</a></li>");
                            } else if(acc["homepage"] == "https://www.youtube.com/") {
                                navigationDiv.append("<li><a onclick='request.buildContent(res[\"onlineAccounts\"]["+i+"]);'>YouTube Data</a></li>");
                            } else {
                                navigationDiv.append("<li><a onclick='request.buildContent(res[\"onlineAccounts\"]["+i+"]);'>" + acc["homepage"] + "</a></li>");
                            }
                            counter++;
                        }
                    }
                }

                navigationDiv.append("<li>Recommendations</li>");

                var contentDiv = request.buildContent(res);
                
                //append elements
                $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'widgets/socialnetwork/hacksAndDemos/personalRecommenderWidget.css') );

                self.html('');
                self.append(headerDiv);
                self.append(navigationDiv);
                self.append(contentDiv);
            });
        });
    };
})( jQuery );

(function( $ ){
    $.fn.build_content = function() {
        var self = $(this);
        res = request.getResource(settings.requested_resource, function(res) {

        });
    };
})( jQuery );