/*
 * Copyright (c) 2012 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function( $ ){
    $.fn.small_login = function() {
        var settings = {
            host: 'http://localhost:8080/LMF/',
            lmflogin : 'social/',
            redirectTo: 'http://localhost:8080/LMF/social/',
            mime_type: 'application/xhtml+xml',
            fblogin : 'auth/oauth2/facebook',
            ytlogin : 'auth/oauth2/youtube'
        }

        /**
         * main method
         */
        return this.each(function() {


            var div = $("<div id='smallLoginForm'></div>");
            var lmfLoginLink = $("<a href=\""+ settings.host + settings.lmflogin +"\">Login</a>");

            var fbURL = '';
            if(settings.redirectTo != '') {
                fbURL = settings.host + settings.fblogin + '?redirect_uri=' + settings.redirectTo;
            } else {
                fbURL = settings.host + settings.fblogin;
            }
            var ytURL = '';
            if(settings.redirectTo != '') {
                ytURL = settings.host + settings.ytlogin + '?redirect_uri=' + settings.redirectTo;
            } else {
                ytURL = settings.host + settings.ytlogin;
            }

            var lmfLoginLink2 = $("<a href=\""+ settings.host + settings.lmflogin +"\"> <img src=\"img/icons/lmf-icon.png\" alt=\"LMF\" width=\"30px\" height=\"30px\"/></a>");

            var fbLoginLink = $("<a href=\""+ fbURL +"\"> <img src=\"img/icons/facebook-icon.png\" alt=\"Facebook\" width=\"30px\" height=\"30px\"/></a>");

            var ytLoginLink = $("<a href=\""+ ytURL +"\"><img src=\"img/icons/youtube-icon.png\" alt=\"Youtube\" width=\"30px\" height=\"30px\"/></a>");

            //append elements
            $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'widgets/socialnetwork/hacksAndDemos/smallLoginWidget.css') );

            $(this).html('');
            div.append(lmfLoginLink);
            div.append(lmfLoginLink2);
            div.append(fbLoginLink);
            div.append(ytLoginLink);
            $(this).append(div);
        });
    };
})( jQuery );

