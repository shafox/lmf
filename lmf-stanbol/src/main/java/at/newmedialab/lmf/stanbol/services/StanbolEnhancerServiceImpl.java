/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.services;

import at.newmedialab.lmf.stanbol.api.SlingLauncherService;
import at.newmedialab.lmf.stanbol.api.StanbolConfigurationService;
import at.newmedialab.lmf.stanbol.api.StanbolEnhancerService;
import at.newmedialab.lmf.stanbol.api.StanbolService;
import at.newmedialab.lmf.stanbol.model.clerezza.SesameMGraph;
import at.newmedialab.lmf.stanbol.model.enhancer.Enhancement;
import at.newmedialab.lmf.stanbol.model.enhancer.EnhancementChain;
import at.newmedialab.lmf.stanbol.model.enhancer.EntityReference;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.google.common.io.ByteStreams;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.qualifiers.cache.MarmottaCache;
import org.apache.stanbol.enhancer.servicesapi.*;
import org.apache.tika.io.IOUtils;
import org.openrdf.query.*;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * TODO: autoconfigure stanbol enhancement chain:
 * curl -v --user admin:admin --data "apply=true&pid=[Temporary PID replaced by real PID upon save]&factoryPid=org.apache.stanbol.enhancer.chain.weighted.impl.WeightedChain&action=ajaxConfigManager&stanbol.enhancer.chain.name=RESTtest&stanbol.enhancer.chain.weighted.chain=tika&stanbol.enhancer.chain.weighted.chain=langid&service.ranking=0&propertylist=stanbol.enhancer.chain.name%2Cstanbol.enhancer.chain.weighted.chain%2Cservice.ranking" http://localhost:8080/system/console/configMgr/
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class StanbolEnhancerServiceImpl implements StanbolEnhancerService {

    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;

    // for access to HTTP Stanbol
    @Inject
    private StanbolService stanbolService;

    // for access to embedded Stanbol
    @Inject
    private SlingLauncherService slingLauncherService;

    @Inject
    private StanbolConfigurationService stanbolConfigurationService;

    @Inject @MarmottaCache("stanbol-cache")
    private Ehcache stanbolCache;


    /**
     * Return the enhancements for the given text using the default enhancement chain.
     *
     * @param text
     * @return
     */
    @Override
    public List<Enhancement> getEnhancements(String text) {
        // find default enhancement chain and then call getEnhancements with it
        for(EnhancementChain chain : stanbolConfigurationService.getEnhancementChains()) {
            if(chain.isDefaultChain()) {
                return getEnhancements(text,chain);
            }
        }

        log.warn("no default enhancement chain found, cannot return enhancements");
        return Collections.emptyList();
    }

    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument.
     *
     * @param text
     * @return
     */
    @Override
    public List<Enhancement> getEnhancements(String text, String chainName) {
        // find enhancement chain with the given name and then call getEnhancements with it
        for(EnhancementChain chain : stanbolConfigurationService.getEnhancementChains()) {
            if(chain.getName().equalsIgnoreCase(chainName)) {
                return getEnhancements(text,chain);
            }
        }

        log.warn("could not find enhancement chain with name {}; cannot return enhancements",chainName);
        return Collections.emptyList();
    }


    private List<Enhancement> getEnhancements(String text, EnhancementChain chain) {
        Hasher hasher = Hashing.goodFastHash(32).newHasher();
        hasher.putString(chain.getEndpoint());
        hasher.putString(text);

        HashCode hashCode = hasher.hash();

        Element cached = stanbolCache.get(hashCode);

        if(cached != null && !stanbolCache.isExpired(cached)) {
            return (List<Enhancement>) cached.getObjectValue();
        } else {
            List<Enhancement> result = new ArrayList<Enhancement>();
            try {
                String selectTextAnnotations = IOUtils.toString(this.getClass().getResourceAsStream("selectTextAnnotations.sparql"));


                Repository data = getEnhancementsRDF(new ByteArrayInputStream(text.getBytes()), "text/plain", chain);

                if(data != null) {
                    RepositoryConnection connection = data.getConnection();
                    connection.begin();

                    // select all text annotations
                    TupleQuery sparqlQuery = (TupleQuery)connection.prepareQuery(QueryLanguage.SPARQL, selectTextAnnotations);

                    TupleQueryResult bindings = sparqlQuery.evaluate();
                    while (bindings.hasNext()) {
                        BindingSet binding = bindings.next();
                        String enhancement = binding.getValue("enhancement").stringValue();
                        String selected    = binding.getValue("selected_text").stringValue();
                        String type        = binding.hasBinding("type") ? binding.getValue("type").stringValue() : null;
                        int start          = Integer.parseInt(binding.getValue("start").stringValue());
                        int end            = Integer.parseInt(binding.getValue("end").stringValue());
                        double confidence  = Double.parseDouble(binding.getValue("confidence").stringValue());

                        Enhancement e = new Enhancement(selected,start,end,confidence, type != null ? new URI(type) : null);


                        // select all entity references
                        String selectEntityReferences = IOUtils.toString(this.getClass().getResourceAsStream("selectEntityAnnotations.sparql")).replace("$ENHANCEMENT$",enhancement);
                        TupleQuery entityReferencesQuery = (TupleQuery)connection.prepareQuery(QueryLanguage.SPARQL, selectEntityReferences);

                        TupleQueryResult bindings2 = entityReferencesQuery.evaluate();
                        while(bindings2.hasNext()) {
                            BindingSet binding2 = bindings2.next();
                            String entity    = binding2.getValue("entity").stringValue();
                            String reference = binding2.getValue("reference").stringValue();
                            String label     = binding2.getValue("label").stringValue();
                            double confidence1  = Double.parseDouble(binding2.getValue("confidence").stringValue());

                            EntityReference r = new EntityReference(label,new URI(reference),confidence1);

                            // select all types of the entity reference
                            String selectEntityTypes = IOUtils.toString(this.getClass().getResourceAsStream("selectTypes.sparql")).replace("$ENTITY$",entity);
                            TupleQuery entityTypesQuery = (TupleQuery)connection.prepareQuery(QueryLanguage.SPARQL, selectEntityTypes);

                            TupleQueryResult bindings3 = entityTypesQuery.evaluate();
                            while(bindings3.hasNext()) {
                                BindingSet binding3 = bindings3.next();

                                String etype = binding3.getValue("type").stringValue();
                                r.addType(new URI(etype));
                            }
                            bindings3.close();
                            e.addEntity(r);

                        }
                        bindings2.close();
                        result.add(e);
                    }
                    bindings.close();


                    connection.commit();
                    connection.close();
                    data.shutDown();
                } else {
                    log.error("Stanbol did not return valid data; check the logfiles for details");
                }
            } catch (RepositoryException e) {
                log.error("error while initialising temporary repository", e);
            } catch (IOException e) {
                log.error("error while retrieving data from Stanbol endpoint: {}", e.getMessage());
            } catch (QueryEvaluationException e) {
                log.error("error while evaluating SPARQL query: {}",e.getMessage());
            } catch (MalformedQueryException e) {
                log.error("error while parsing SPARQL query; this is a programming error, please report it",e);
            } catch (URISyntaxException e) {
                log.error("returned enhancement type was not a URI");
            }

            stanbolCache.put(new Element(hashCode,result));

            return result;
        }
    }


    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument as
     * Sesame RDF repository for further processing.
     *
     * @param text
     * @return
     */
    @Override
    public Repository getEnhancementsRDF(String text) {
        return getEnhancementsRDF(new ByteArrayInputStream(text.getBytes()), "text/plain");
    }

    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument as
     * Sesame RDF repository for further processing.
     *
     * @param text
     * @param chain
     * @return
     */
    @Override
    public Repository getEnhancementsRDF(String text, String chain) {
        return getEnhancementsRDF(new ByteArrayInputStream(text.getBytes()), "text/plain", chain);
    }

    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument as
     * Sesame RDF repository for further processing.
     *
     *
     * @param content an input stream to read the content from; will be closed when reading is finished
     * @param contentType
     * @return
     */
    @Override
    public Repository getEnhancementsRDF(InputStream content, String contentType) {
        // find default enhancement chain and then call getEnhancements with it
        for(EnhancementChain chain : stanbolConfigurationService.getEnhancementChains()) {
            if(chain.isDefaultChain()) {
                return getEnhancementsRDF(content, contentType, chain);
            }
        }

        log.warn("could not find default enhancement chain; cannot return enhancements");
        return null;
    }

    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument as
     * Sesame RDF repository for further processing.
     *
     *
     * @param content an input stream to read the content from; will be closed when reading is finished
     * @param contentType
     *@param chainName  @return
     */
    @Override
    public Repository getEnhancementsRDF(InputStream content, String contentType, String chainName) {
        // find enhancement chain with the given name and then call getEnhancements with it
        for(EnhancementChain chain : stanbolConfigurationService.getEnhancementChains()) {
            if(chain.getName().equalsIgnoreCase(chainName)) {
                return getEnhancementsRDF(content, contentType, chain);
            }
        }

        log.warn("could not find enhancement chain with name {}; cannot return enhancements",chainName);
        return null;
    }



    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument as
     * Sesame RDF repository for further processing.
     *
     *
     * @param content an input stream to read the content from; will be closed when reading is finished
     * @param contentType
     * @param chain
     * @return
     */
    private Repository getEnhancementsRDF(final InputStream content, final String contentType, EnhancementChain chain) {
        // different ways of getting the enhancements depending on whether we use the embedded stanbol or a remote stanbol

        // get the relevant Stanbol services from embedded Stanbol
        ContentItemFactory ciFactory = slingLauncherService.getContentItemFactory();

        if(ciFactory == null) {
            log.error("error retrieving content item factory from embedded Stanbol; aborting");
            return null;
        }

        EnhancementJobManager enhancementJobManager = slingLauncherService.getEnhancementJobManager();

        if(enhancementJobManager == null) {
            log.error("error retrieving enhancement job manager from embedded Stanbol; aborting");
            return null;
        }

        ChainManager chainManager = slingLauncherService.getChainManager();

        if(chainManager == null) {
            log.error("error retrieving chain manager from embedded Stanbol; aborting");
            return null;
        }


        Repository repository = new SailRepository(new MemoryStore());
        try {
            repository.initialize();

            // this is the graph we use for the content item
            SesameMGraph sesameGraph = new SesameMGraph(repository);

            // and this is the content source we create out of the input stream
            ContentSource contentSource = new ContentSource() {
                @Override
                public InputStream getStream() {
                    return content;
                }

                @Override
                public byte[] getData() throws IOException {
                    return ByteStreams.toByteArray(content);
                }

                @Override
                public String getMediaType() {
                    return contentType;
                }

                @Override
                public String getFileName() {
                    return null;  //To change body of implemented methods use File | Settings | File Templates.
                }

                @Override
                public Map<String, List<String>> getHeaders() {
                    return Collections.emptyMap();
                }
            };

            // work around poor performance of MemoryStore
            ContentItem ci = ciFactory.createContentItem(configurationService.getBaseUri()+"enhancement/",contentSource);
            enhancementJobManager.enhanceContent(ci,chainManager.getChain(chain.getName()));
            // work around poor performance of MemoryStore
            sesameGraph.addAll(ci.getMetadata());
            return repository;
        } catch (RepositoryException e) {
            log.error("error initialising Sesame in-memory repository",e);
            return null;
        } catch (IOException e) {
            log.error("error while reading stream content",e);
            return null;
        } catch (EnhancementException e) {
            log.error("error while enhancing content",e);
            return null;
        }

    }

}
