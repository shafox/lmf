package at.newmedialab.lmf.stanbol.services;

import at.newmedialab.lmf.stanbol.api.SlingLauncherService;
import at.newmedialab.lmf.stanbol.api.StanbolConfigurationService;
import at.newmedialab.lmf.stanbol.api.TopicClassifierService;
import at.newmedialab.lmf.stanbol.exception.EngineAlreadyExistsException;
import at.newmedialab.lmf.stanbol.model.config.TopicClassificationEngineConfig;
import at.newmedialab.stanbol.configurator.model.LMFTopicClassificationTrainingSet;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.marmotta.commons.util.HashUtils;
import org.apache.marmotta.kiwi.transactions.model.TransactionData;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.qualifiers.event.transaction.AfterCommit;
import org.apache.stanbol.enhancer.topic.api.ClassifierException;
import org.apache.stanbol.enhancer.topic.api.TopicClassifier;
import org.apache.stanbol.enhancer.topic.api.TopicSuggestion;
import org.apache.stanbol.enhancer.topic.api.training.TrainingSet;
import org.apache.stanbol.enhancer.topic.api.training.TrainingSetException;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.SKOS;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
@ApplicationScoped
public class TopicClassifierServiceImpl implements TopicClassifierService {

    @Inject
    private Logger log;

    @Inject
    private SesameService sesameService;

    @Inject
    private SlingLauncherService slingLauncherService;

    @Inject
    private StanbolConfigurationService stanbolConfigurationService;

    @Inject
    private ConfigurationService configurationService;

    /**
     * Create a topic classifier using the given name and synch'ed with the given context. This method will
     * automatically create a topic classifier and a training set, and add configuration entries into the
     * system configuration to allow automated synch'ing of vocabulary updates.
     *
     * @param context
     */
    @Override
    public void createTopicClassifier(URI context) throws ClassifierException, RepositoryException {
        try {
            LMFTopicClassificationTrainingSet trainingSetConfig = new LMFTopicClassificationTrainingSet(getTrainingSetName(context), getTrainingSetName(context) + "model");
            stanbolConfigurationService.createTopicClassificationTrainingSet(trainingSetConfig);
        } catch (EngineAlreadyExistsException e) {
            log.error("training set for context {} already exists, ignoring...");
        }

        try {
            TopicClassificationEngineConfig engineConfig = new TopicClassificationEngineConfig(getClassifierName(context), getClassifierName(context) + "-model", getTrainingSetName(context));
            stanbolConfigurationService.createTopicClassificationEngine(engineConfig);
        } catch (EngineAlreadyExistsException e) {
            log.error("topic classification engine for context {} already exists, ignoring...");
        }

        addSkosVocabulary(getClassifierName(context), sesameService.getConnection(), context, true);

        List<String> topicContexts = configurationService.getListConfiguration("stanbol.topic_contexts");
        if(!topicContexts.contains(context.stringValue())) {
            topicContexts.add(context.stringValue());
            configurationService.setListConfiguration("stanbol.topic_contexts", topicContexts);
        }

    }

    /**
     * Remove the topic classifier using the given name and synch'ed with the given context. This method will
     * automatically create a topic classifier and a training set, and add configuration entries into the
     * system configuration to allow automated synch'ing of vocabulary updates.
     *
     * @param context
     */
    @Override
    public void removeTopicClassifier(URI context) throws ClassifierException, RepositoryException {

    }

    /**
     * React on transaction commit and synchronize the transaction data of the published contexts.
     *
     * @param data
     */
    public void updateClassifiers(@Observes @AfterCommit final TransactionData data) {
        for(String context : configurationService.getListConfiguration("stanbol.topic_contexts")) {
            URI contextUri = new URIImpl(context);
            if(data.getAddedTriples().listTriples(null,null,null,contextUri,false).size() > 0 ||
               data.getRemovedTriples().listTriples(null,null,null,contextUri,false).size() > 0) {
                try {
                    addSkosVocabulary(getClassifierName(contextUri), sesameService.getConnection(), contextUri, true);
                } catch (ClassifierException e) {
                    log.error("error finding classifier for context {}", context);
                } catch (RepositoryException e) {
                    log.error("error accessing RDF repository",e);
                }
            }
        }
    }

    /**
     * Return the names of all topic classifiers configured in the system.
     *
     * @return
     */
    @Override
    public List<String> getTopicClassifiers() {
        return Lists.transform(slingLauncherService.getTopicClassifiers(), new Function<TopicClassifier, String>() {
            @Override
            public String apply(TopicClassifier input) {
                return input.getName();
            }
        });
    }

    /**
     * Return the training set with the name given as argument
     *
     * @param name
     * @return
     */
    @Override
    public TrainingSet getTrainingSet(String name) {
        for(TrainingSet trainingSet : slingLauncherService.getTrainingSets()) {
            if(StringUtils.equals(name, trainingSet.getName())) {
                return trainingSet;
            }
        }

        return null;
    }

    /**
     * Return the training set for the vocabulary with the given context.
     *
     * @param context
     * @return
     */
    @Override
    public TrainingSet getTrainingSet(Resource context) {
        return getTrainingSet(getTrainingSetName(context));
    }

    /**
     * Return the topic classifier with the given name as argument.
     *
     * @param name
     * @return
     */
    @Override
    public TopicClassifier getTopicClassifier(String name) {
        for(TopicClassifier classifier : slingLauncherService.getTopicClassifiers()) {
            if(StringUtils.equals(name,classifier.getName())) {
                return classifier;
            }
        }

        return null;
    }

    /**
     * Return the topic classifier for the vocabulary with the given context..
     *
     * @param context
     * @return
     */
    @Override
    public TopicClassifier getTopicClassifier(Resource context) {
        return getTopicClassifier(getClassifierName(context));
    }

    /**
     * Suggest topics for the text passed as argument using the specified classifier
     *
     * @param classifierName
     * @param text
     * @return
     */
    @Override
    public List<TopicSuggestion> suggestTopics(String classifierName, String text) throws ClassifierException {
        TopicClassifier classifier = getTopicClassifier(classifierName);
        if(classifier == null) {
            throw new ClassifierException("classifier with name "+classifierName+" does not exist");
        }
        return classifier.suggestTopics(text);
    }

    /**
     * Suggest topics for the text passed as argument using the specified classifier
     *
     * @param vocabulary
     * @param text
     * @return
     */
    @Override
    public List<TopicSuggestion> suggestTopics(Resource vocabulary, String text) throws ClassifierException {
        return suggestTopics(getClassifierName(vocabulary), text);
    }

    /**
     * Add all concepts from the SKOS vocabulary available through the given repository connection. This method
     * can also be used with an InterceptingRepositoryConnection to filter on the Marmotta repository
     *
     * @param connection
     */
    @Override
    public void addSkosVocabulary(String classifierName, RepositoryConnection connection) throws ClassifierException {
        addSkosVocabulary(classifierName, connection, null, false);
    }


    /**
     * Add all concepts from the SKOS vocabulary available in the given Sesame context in the underlying Marmotta
     * repository.
     *
     * @param context
     */
    @Override
    public void addSkosVocabulary(String classifier, Resource context) throws RepositoryException, ClassifierException {
        addSkosVocabulary(classifier, sesameService.getConnection(), context, false);
    }

    /**
     * Add all concepts from the SKOS vocabulary available through the given repository connection. This method
     * can also be used with an InterceptingRepositoryConnection to filter on the Marmotta repository
     *
     * @param connection
     */
    private void addSkosVocabulary(String classifierName, RepositoryConnection connection, Resource context, boolean remove) throws ClassifierException {
        try {
            TopicClassifier classifier = null;

            while(classifier == null) {
                classifier = getTopicClassifier(classifierName);
                if(classifier == null) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                    }
                }
            }
            if(classifier == null) {
                throw new ClassifierException("classifier with name "+classifierName+" does not exist");
            }

            if(remove) {
                classifier.removeAllConcepts();
            }

            Set<Resource> concepts = new HashSet<>();

            RepositoryResult<Statement> conceptStmts = context != null ? connection.getStatements(null, RDF.TYPE, SKOS.CONCEPT, true, context) : connection.getStatements(null, RDF.TYPE, SKOS.CONCEPT, true);
            while (conceptStmts.hasNext()) {
                concepts.add(conceptStmts.next().getSubject());
            }
            conceptStmts.close();

            for(Resource concept : concepts) {
                Set<String> broader = new HashSet<>();

                RepositoryResult<Statement> broaderStmts = context != null ? connection.getStatements(concept, SKOS.BROADER, null, true, context) : connection.getStatements(concept, SKOS.BROADER, null, true);
                while(broaderStmts.hasNext()) {
                    Value object = broaderStmts.next().getObject();
                    if(object instanceof Resource) {
                        broader.add(object.stringValue());
                    }
                }
                broaderStmts.close();

                classifier.addConcept(concept.stringValue(), broader);
            }


            if(remove) {
                classifier.updateModel(false);
            }

            connection.commit();
            connection.close();
        } catch (RepositoryException e) {
            log.error("repository error publishing SKOS vocabulary to classifier",e);
        } catch (TrainingSetException e) {
            log.error("error updating training set data", e);
        }
    }

    public String getClassifierName(Resource uri) {
        return "lmf-classifier-"+ StringUtils.capitalize(HashUtils.md5sum(uri.stringValue()));
    }

    public String getTrainingSetName(Resource uri) {
        return "lmf-trainer-"+ StringUtils.capitalize(HashUtils.md5sum(uri.stringValue()));
    }

}
