/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.services;

import at.newmedialab.lmf.stanbol.api.SlingLauncherService;
import at.newmedialab.stanbol.EmbeddedStanbol;
import at.newmedialab.stanbol.StanbolConfiguration;
import at.newmedialab.stanbol.configurator.api.ConfiguratorService;
import at.newmedialab.stanbol.entityhub.api.EntityhubHelper;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.events.ConfigurationChangedEvent;
import org.apache.marmotta.platform.core.events.ConfigurationServiceInitEvent;
import org.apache.stanbol.enhancer.servicesapi.ChainManager;
import org.apache.stanbol.enhancer.servicesapi.ContentItemFactory;
import org.apache.stanbol.enhancer.servicesapi.EnhancementEngineManager;
import org.apache.stanbol.enhancer.servicesapi.EnhancementJobManager;
import org.apache.stanbol.enhancer.topic.api.TopicClassifier;
import org.apache.stanbol.enhancer.topic.api.training.TrainingSet;
import org.apache.stanbol.entityhub.servicesapi.Entityhub;
import org.apache.stanbol.entityhub.servicesapi.site.SiteManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.List;

/**
 /**
 * This service starts up an embedded Sling installation for Apache Stanbol in case the configuration option
 * stanbol.embedded.enabled is set to true. Sling bundles an embedded Jetty server which will
 * start up on port stanbol.embedded.port (default 8081).
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class SlingLauncherServiceImpl implements SlingLauncherService {


    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;

//    @Inject
//    private LMFWebConsoleSecurityIntegrator securityIntegrator;

    private EmbeddedStanbol stanbol;

    private boolean started = false;

    public void startup(@Observes ConfigurationServiceInitEvent event) {
        // trigger initialisation
    }



    /**
     * Startup and configure an embedded Apache Sling instance and all OSGi bundles. Set the appropriate LMF
     * configuration options.
     */
    @Override
    @PostConstruct
    public void initialise() {
        if(!started) {
            log.info("Starting up LMF Sling Launcher ...");

            StanbolConfiguration config = new StanbolConfiguration(configurationService.getHome());
            config.setSecurityRealm("Linked Media Framework");

            this.stanbol = new EmbeddedStanbol(config);

// OSGI... no comment...            
//            // Enable security
//            Dictionary<String, Object> settings = new Hashtable<String, Object>();
//            settings.put(SERVICE_RANKING, Integer.MAX_VALUE);
//            this.stanbol.getBundleContext().registerService(WebConsoleSecurityProvider.class.getName(), securityIntegrator, settings);


            // since Stanbol embedded is enabled, we reconfigure HTTP Stanbol access to the local installation
            configurationService.setConfiguration("stanbol.path",stanbol.getSlingHome());
            configurationService.setConfiguration("stanbol.uri", configurationService.getServerUri() + "stanbol/config/");


            // register bundle context in servlet context
            if(configurationService.getServletContext() != null) {
                configurationService.getServletContext().setAttribute(BundleContext.class.getName(), stanbol.getBundleContext());
            }
        }
    }

    public void configurationChangedEvent(@Observes ConfigurationChangedEvent event) {
        if(event.containsChangedKey("kiwi.host")) {
            configurationService.setConfiguration("stanbol.uri", configurationService.getServerUri() + "stanbol/config/");
        }
    }

    /**
     * Shutdown the embedded Apache Sling instance and all bundles.
     */
    @Override
    @PreDestroy
    public void shutdown() {
        this.stanbol.shutdown();

        // further cleanup
        this.started = false;
    }

    /**
     * Return the URL where the embedded Stanbol installation is running.
     * @return
     */
    @Override
    public String getEmbeddedStanbolURL() {
        //return "http://"+configurationService.getStringConfiguration("stanbol.embedded.host","localhost")+":"+configurationService.getStringConfiguration("stanbol.embedded.port", "8081");
        return configurationService.getStringConfiguration("stanbol.uri");
    }


    @Override
    public boolean ping() {
        return stanbol.ping();
    }

    /**
     * Return the configuration helper service that can be used to create typical stanbol configurations
     * @return
     */
    @Override
    public ConfiguratorService getConfiguratorService() {
        return stanbol.getConfiguratorService();
    }

    /**
     * Return the chain manager of the embedded stanbol installation if available, or null otherwise.
     * The chain manager can provide information about the different enhancement chains.
     * @return
     */
    @Override
    public ChainManager getChainManager() {
        return stanbol.getChainManager();
    }

    /**
     * Return the enhancement job manager of the embedded stanbol installation if available, or null otherwise.
     * The enhancement job manager can be used for running the different enhancement chains directly.
     * @return
     */
    @Override
    public EnhancementJobManager getEnhancementJobManager() {
        return stanbol.getEnhancementJobManager();
    }

    /**
     * Return the engine manager of the embedded stanbol installation if available, or null otherwise.
     * The engine manager can provide information about the different enhancement engines.
     * @return
     */
    @Override
    public EnhancementEngineManager getEngineManager() {
        return stanbol.getEngineManager();
    }

    /**
     * Return the content item factory of the embedded stanbol installation if available, or null otherwise.
     * The content item factory provides methods for injecting content into Stanbol.
     * @return
     */
    @Override
    public ContentItemFactory getContentItemFactory() {
        return stanbol.getContentItemFactory();
    }


    /**
     * Return the helper service used to create entityhub representations. This is a bridging service used for accessing
     * Stanbol internals.
     * @return
     */
    @Override
    public EntityhubHelper getEntityhubHelper() {
        return stanbol.getEntityhubHelper();
    }

    /**
     * Return the stanbol site manager, used for listing and accessing Stanbol sites.
     * @return
     */
    @Override
    public SiteManager getSiteManager() {
        return stanbol.getSiteManager();
    }


    @Override
    public Entityhub getEntityhub() {
        return stanbol.getEntityhub();
    }


    /**
     * Return the bundle with the given symbolic name.
     *
     * @param symbolicName
     * @return
     */
    @Override
    public Bundle getBundle(String symbolicName) {
        return stanbol.getBundle(symbolicName);
    }


    @Override
    public List<TopicClassifier> getTopicClassifiers() {
        return stanbol.getTopicClassifiers();
    }

    @Override
    public List<TrainingSet> getTrainingSets() {
        return stanbol.getTrainingSets();
    }
}
