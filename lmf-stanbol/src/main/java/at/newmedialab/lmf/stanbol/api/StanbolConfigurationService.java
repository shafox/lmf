/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.api;

import at.newmedialab.lmf.stanbol.exception.ChainAlreadyExistsException;
import at.newmedialab.lmf.stanbol.exception.EngineAlreadyExistsException;
import at.newmedialab.lmf.stanbol.exception.SiteAlreadyExistsException;
import at.newmedialab.lmf.stanbol.model.config.*;
import at.newmedialab.lmf.stanbol.model.enhancer.EnhancementChain;
import at.newmedialab.lmf.stanbol.model.enhancer.EnhancementEngine;
import at.newmedialab.stanbol.configurator.model.LMFTopicClassificationTrainingSet;

import java.util.Map;
import java.util.Set;

/**
 * This service provides functionality for configuring Apache Stanbol. It can be used to create different kinds of
 * engines, enhancement chains and managed / referenced sites.
 * <p/>
 * Author: Sebastian Schaffert
 */
public interface StanbolConfigurationService {


    /**
     * Return a list of pre-configured enhancement engines available for installation.
     * @return
     */
    public Map<String,StanbolEngineConfiguration> getAvailableEnhancementEngines();


    /**
     * Return a list of pre-configured enhancement chains available for installation.
     * @return
     */
    public Map<String,StanbolChainConfiguration> getAvailableEnhancementChains();


    /**
     * Return a list of all enhancement chains installed in stanbol.
     *
     * @return
     */
    public Set<EnhancementChain> getEnhancementChains();


    /**
     * Return a list of all enhancement engines installed in Stanbol.
     * @return
     */
    public Set<EnhancementEngine> getEnhancementEngines();



    /**
     * Create the keyword linking engine with the given specification. Should check whether an engine with this name
     * already exists and throw an exception.
     * @param engine
     * @return true if the engine has been created successfully
     */
    public boolean createKeywordLinkingEngine(KeywordLinkingEngineConfig engine) throws EngineAlreadyExistsException;


    /**
     * Create the named entity tagging engine with the given specification. Should check whether an engine with this name
     * already exists and throw an exception.
     * @param engine
     * @return true if the engine has been created successfully
     */
    public boolean createEntityTaggingEngine(EntityTaggingEngineConfig engine) throws EngineAlreadyExistsException;


    /**
     * Create the list chain with the given specification. Should check whether a chain with this name
     * already exists and throw an exception.
     *
     * @param config
     * @return true if the engine has been created successfully
     */
    public boolean createListChain(ListChainConfiguration config) throws ChainAlreadyExistsException;


    /**
     * Create a managed site according to the configuration passed as argument. Throws an exception in case the site
     * already exists.
     * @param config
     * @throws SiteAlreadyExistsException
     */
    public boolean createManagedSite(ManagedSiteConfiguration config) throws SiteAlreadyExistsException;


    /**
     * Asynchronously install the referenced site passed as argument. Will start a separate thread and return.
     * @param configuration
     */
    public void createReferencedSite(ReferencedSiteConfiguration configuration);


    /**
     * Create a new topic classification engine with the specification given as argument.
     *
     *
     * @param config
     * @throws EngineAlreadyExistsException
     */
    public boolean createTopicClassificationEngine(TopicClassificationEngineConfig config) throws EngineAlreadyExistsException;


    /**
     * Create a new topic classification training set. This training set is required for using a topic classification
     * engine.
     *
     * @param config
     * @throws EngineAlreadyExistsException
     */
    public boolean createTopicClassificationTrainingSet(LMFTopicClassificationTrainingSet config) throws EngineAlreadyExistsException;
    /**
     * Delete the managed site with the ID given as argument.
     *
     * @param siteId
     * @return
     */
    public boolean deleteManagedSite(String siteId);

    /**
     * Create the entityhub linking engine with the given specification. Should check whether an engine with this name
     * already exists and throw an exception.
     *
     * @param engine
     * @return true if the engine has been created successfully
     */
    boolean createEntityhubLinkingEngine(EntityhubLinkingEngineConfig engine) throws EngineAlreadyExistsException;
}
