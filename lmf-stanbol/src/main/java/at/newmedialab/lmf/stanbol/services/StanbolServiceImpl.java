/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.services;

import at.newmedialab.lmf.stanbol.api.SlingLauncherService;
import at.newmedialab.lmf.stanbol.api.StanbolService;
import at.newmedialab.lmf.stanbol.events.StanbolInitEvent;
import at.newmedialab.lmf.stanbol.exception.DeploymentFailedException;
import at.newmedialab.stanbol.configurator.api.ConfiguratorService;
import at.newmedialab.stanbol.configurator.exception.ConfiguratorException;
import net.sf.ehcache.Ehcache;
import org.apache.commons.io.FileUtils;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.qualifiers.cache.MarmottaCache;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

/**
 *
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class StanbolServiceImpl implements StanbolService {


    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;


    @Inject @MarmottaCache("stanbol-cache")
    private Ehcache stanbolCache;


    @Inject
    @Any
    private Event<StanbolInitEvent> stanbolInitEvent;

    @Inject
    private SlingLauncherService slingLauncherService;


    @PostConstruct
    public void initialise() {

        if(slingLauncherService.getConfiguratorService() != null) {
            try {
                createDeploymentDirectory();
            } catch (ConfiguratorException e) {
                log.warn("could not create deployment directory",e);
            }
        }
    }


    /**
     * Check whether the Stanbol server is ready. Issues an HTTP request to the /sites endpoint
     *
     * @return
     */
    @Override
    public boolean ping() {
            return slingLauncherService.ping();
    }


    @Override
    public String getStanbolUri() {
        return  configurationService.getStringConfiguration("stanbol.uri", configurationService.getServerUri() + "/stanbol/config/");
    }


    /**
     * Return the directory where Stanbol stores its data.
     *
     * @return
     */
    @Override
    public String getStanbolHome() {
        try {
            String confHome = configurationService.getStringConfiguration("stanbol.path");
            if(confHome != null && !"".equals(confHome)) {
                // relative or absolute?
                File fileHome = new File(confHome);
                if(fileHome.isAbsolute())
                    return fileHome.getCanonicalPath();
                else {
                    fileHome = new File(configurationService.getLMFHome() + File.separator + confHome);
                    return fileHome.getCanonicalPath();
                }

            } else
                return configurationService.getLMFHome() + File.separator + "stanbol";
        } catch (IOException e) {
            log.error("I/O error resolving Stanbol home directory: {}", e.getMessage());
            return null;
        }
    }

    /**
     * Create the OSGi deployment directory in case the configuration manager is available.
     *
     * @return
     * @throws ConfiguratorException
     */
    private File createDeploymentDirectory() throws ConfiguratorException {
        File deploymentDirectory = new File(getStanbolHome()+File.separator+"deploy");
        if(!deploymentDirectory.exists()) {
            deploymentDirectory.mkdirs();
        }
        if(slingLauncherService.getConfiguratorService() != null) {
            // create deployment directory

            // ensure the deployment directory is configured
            ConfiguratorService cfgAdmin = slingLauncherService.getConfiguratorService();
            cfgAdmin.createBundleDeploymentConfiguration(deploymentDirectory.getAbsolutePath());

        } else {
            log.warn("Stanbol Configurator Service not available, automatic deployment will not work");
        }
        return deploymentDirectory;
    }

    /**
     * Deploy an OSGi bundle to the Apache Stanbol container. Used e.g. to install referenced site configurations.
     * The method expects a file as argument. The file will be moved to the deployment directory of the Felix File
     * Installer.
     *
     * @param bundleLocation
     */
    @Override
    public void deployBundle(File bundleLocation) throws DeploymentFailedException {
        try {

            // create deployment directory
            File deploymentDirectory = createDeploymentDirectory();

            FileUtils.moveFileToDirectory(bundleLocation, deploymentDirectory, false);

        } catch (ConfiguratorException e) {
            log.error("could not create deployment configuration in OSGi container",e);
            throw new DeploymentFailedException("could not create deployment configuration in OSGi container",e);
        } catch (IOException e) {
            log.error("could not move bundle jar file to deployment directory",e);
            throw new DeploymentFailedException("could not move bundle jar file to deployment directory",e);
        }

    }

    /**
     * Flush the internal enhancement cache (e.g. in case the entity chains have been republished)
     */
    @Override
    public void flushCache() {
        stanbolCache.removeAll();
    }

}
