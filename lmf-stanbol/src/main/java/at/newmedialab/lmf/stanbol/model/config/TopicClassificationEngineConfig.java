package at.newmedialab.lmf.stanbol.model.config;

import at.newmedialab.stanbol.configurator.model.LMFTopicClassificationEngine;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class TopicClassificationEngineConfig extends LMFTopicClassificationEngine implements StanbolEngineConfiguration {


    public TopicClassificationEngineConfig(String name, String core, String trainingSet) {
        super(name, core, trainingSet);
    }

    /**
     * Return the name of this web configuration
     *
     * @return
     */
    @Override
    public String getId() {
        return getName();
    }

    /**
     * Return the name of the site this engine refers to.
     *
     * @return
     */
    @Override
    public String getSite() {
        return getTrainingSet();
    }

    /**
     * Change the name of the site this engine refers to
     *
     * @param siteName
     */
    @Override
    public void setSite(String siteName) {
        setTrainingSet(siteName);
    }

    /**
     * Update the name of this web configuration
     *
     * @param id
     */
    @Override
    public void setId(String id) {
        setName(id);
    }
}
