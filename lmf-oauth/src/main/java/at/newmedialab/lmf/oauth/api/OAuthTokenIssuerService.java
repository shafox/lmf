/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.api;

import kiwi.core.model.user.KiWiUser;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

/**
 * @author Stephanie Stroka
 *         User: sstroka
 *         Date: 27.09.2011
 *         Time: 10:28:06
 */
public interface OAuthTokenIssuerService {

    public Response constructOAuthResponse(HttpServletRequest request, KiWiUser user, String redirect_uri);

    public Response refreshOAuthToken(String refreshToken);
}
