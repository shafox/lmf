<?php
namespace LMFClient\Model\Content;


/**
 * Created by IntelliJ IDEA.
 * User: sschaffe
 * Date: 25.01.12
 * Time: 14:51
 * To change this template use File | Settings | File Templates.
 */
class Content
{
    /** @var string the buffer containing the data */
    private $data;

    /** @var integer the size of the content */
    private $size;

    /** @var string the mime type of the content */
    private $mimetype;

    function __construct($stream,$size,$mimetype)
    {
        $this->data     = $stream;
        $this->size     = $size;
        $this->mimetype = $mimetype;
    }

    /**
     * @return string
     */
    public function getMimetype()
    {
        return $this->mimetype;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }


}
