<?php
namespace LMFClient\Exceptions;

require_once 'LMFClientException.php';

use LMFClient\Exceptions\LMFClientException;

/**
 * Created by IntelliJ IDEA.
 * User: sschaffe
 * Date: 27.01.12
 * Time: 10:04
 * To change this template use File | Settings | File Templates.
 */
class NotFoundException extends LMFClientException
{

}
