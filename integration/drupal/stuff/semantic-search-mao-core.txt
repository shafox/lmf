
@prefix mao : <http://www.w3.org/ns/ma-ont#> ;
@filter rdf:type is mao:VideoTrack ;
  title = mao:title :: lmf:text_en ;
  summary = mao:description :: lmf:text_en ;
  tag = mao:hasKeyword :: lmf:text_en ;

