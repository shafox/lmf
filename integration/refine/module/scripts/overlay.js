
//LMF overlay
$(function() {
    
	//Extensions toolbar
    //ExtensionBar.MenuItems.reconcile.submenu.push()
    ExtensionBar.MenuItems.push({
        "id": "lmf",
        "label": "LMF",
        "submenu" : [
			{
			    "id": "lmf-configure",
			    label: "LMF Configuration",
			    click: function() { 
			        var dialog = new LMFConfigurationDialog(); 
			        dialog.show();
			    }
			},
			/*
            {
                "id": "lmf-stanbol-reconciliation-configuration",
                label: "Preconfigure Stanbol-based Reconciliation",
                click: function() { 
                    var dialog = new StanbolReconciliationConfiguration(); 
                    dialog.show();
                }
			},
			*/
            {
                "id": "lmf-save",
                label: "Save to LMF",
                click: function() { 
                	if (!theProject.overlayModels.lmf) {
                		alert("You still haven't configured LMF!\n Please, do it before save your data.")
                	} else {
                    	LMF.save();
                	}
                }
            },
            {
                "id": "lmf-go",
                label: "Go to LMF",
                click: function() { 
                    LMF.go(); 
                }
            }
        ]
	});

    //logo
    $("a#app-home-button").children("img").attr("src", "extension/lmf/images/lmf_refine_logo_30px.png").attr("width", "282");

    //favicon
    $("link[rel$=icon]").replaceWith("");
    $("<link rel=shortcut icon type='image/png' />").appendTo("head").attr("href", "extension/lmf/images/favicon.png");

    //project controls
    $("div#project-controls a:first-child").text("Home");
    $("div#project-controls a:eq(2)").attr("href", "http://code.google.com/p/lmf/wiki/GoogleRefineExtension");

    //overwrites the original function, injecting LMF's base URI
    RdfExporterMenuBar.editRdfSchema = function(reset) {
        var schema = (reset ? null : theProject.overlayModels.rdfSchema);
        if (reset || schema==null) {
        	var uri = LMF.getBaseUri();
        	if (uri !== undefined) {
	            schema = { 
	                rootNodes: [],
	                baseUri: LMF.getBaseUri()
	            };
        	}
        }
        new RdfSchemaAlignmentDialog(schema);
    };

});

