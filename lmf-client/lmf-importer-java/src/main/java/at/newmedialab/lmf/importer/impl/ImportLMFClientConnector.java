/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.newmedialab.lmf.client.ClientConfiguration;
import at.newmedialab.lmf.client.clients.ContextClient;
import at.newmedialab.lmf.client.clients.ImportClient;
import at.newmedialab.lmf.client.clients.ResourceClient;
import at.newmedialab.lmf.importer.api.LMFClientConnector;

/**
 * An LMF Client Connector based on the Import webservice. The implementation will first remove
 * the updated resource using the resource webservice and then add the new data using the import
 * webservice.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class ImportLMFClientConnector implements LMFClientConnector {

    private static Logger log = LoggerFactory.getLogger(ImportLMFClientConnector.class);

    private final String uri;
    private final String user;
    private final String pass;

    private BasicHttpParams httpParams;
    private PoolingClientConnectionManager connectionManager;

    public ImportLMFClientConnector(String uri, String user, String pass) {
        super();
        this.uri = uri;
        this.user = user;
        this.pass = pass;

        initHttpClient();
    }

    private void initHttpClient() {
        httpParams = new BasicHttpParams();
        httpParams.setParameter(CoreProtocolPNames.USER_AGENT, "SNML SN Importer");
        httpParams.setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, true);
        httpParams.setIntParameter(ClientPNames.MAX_REDIRECTS, 3);
        httpParams.setBooleanParameter(CoreConnectionPNames.SO_KEEPALIVE, true);

        connectionManager = new PoolingClientConnectionManager();
        connectionManager.setMaxTotal(50);
        connectionManager.setDefaultMaxPerRoute(50);
    }

    private ClientConfiguration buildConfiguration(String context) {
        ClientConfiguration conf = new ClientConfiguration(uri);
        conf.setConectionManager(connectionManager);
        if (StringUtils.isNotBlank(user)) {
            conf.setLmfUser(user);
        }
        if (StringUtils.isNotBlank(pass)) {
            conf.setLmfPassword(pass);
        }
        if (StringUtils.isNotBlank(context)) {
            conf.setLmfContext(context);
        }
        return conf;
    }

    public PoolingClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    /**
     * Update the news item with the URI given as first argument using the data (all triples) provided by the repository
     * connection given as third argument. The update will be carried out in the context (named graph) with the URI given
     * as second argument.
     *
     * @param itemUri    the URI of the news item (news article, wiki article, blog posting, image, video) to update
     * @param contextUri the context URI where to store the triples in the LMF
     * @param connection the repository connection containing the updated triples
     * @throws java.io.IOException in case there is an error connecting to the LMF
     * @throws org.openrdf.repository.RepositoryException
     *                             in case there is an error accessing the triples
     */
    @Override
    public boolean updateContentItem(URI itemUri, URI contextUri, RepositoryConnection connection) throws IOException, RepositoryException {
        log.debug("Saving item '{}' ...", itemUri);

        ImportClient importClient = new ImportClient(buildConfiguration(contextUri.stringValue()));
        RDFFormat format = RDFFormat.TURTLE;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            RDFWriter w = Rio.createWriter(format, out);
            connection.export(w);
            InputStream is = new ByteArrayInputStream(out.toByteArray());
            importClient.uploadDataset(is, format.getDefaultMIMEType(), contextUri.stringValue());
            log.info("Article '{}' successfully imported into LMF", itemUri);
            return true;
        } catch (Exception e) {
            log.error("Error saving data as " + format + " into LMF: " + e.getMessage());
            return false;
        }
    }
    
	@Override
	public boolean deleteContentItem(URI itemUri) {
		log.debug("Deleting item '{}' ...", itemUri.stringValue());
        ResourceClient resourceClient = new ResourceClient(buildConfiguration(itemUri.stringValue()));
        try {
			resourceClient.deleteResource(itemUri.stringValue());
			return true;
		} catch (IOException e) {
			log.error("Error deleting item {}: {}", itemUri.stringValue(), e.getMessage());
			return false;
		}
	}

	@Override
	public boolean resetContext(URI contextUri) throws IOException, RepositoryException {
        ContextClient client = new ContextClient(buildConfiguration(contextUri.stringValue()));
        try {
            return client.delete(contextUri.stringValue());
        } catch (Exception e) {
            return false;
        }
	}
	
}
