/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.openrdf.model.URI;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.newmedialab.lmf.importer.model.VocabularyManager;
import at.newmedialab.lmf.importer.util.ProcessingJob;
import at.newmedialab.lmf.importer.util.ProcessingTracker;
import at.newmedialab.lmf.util.geonames.api.GeoLookup;
import at.newmedialab.lmf.util.geonames.builder.GeoLookupBuilder;
import de.schlichtherle.truezip.file.TFile;
import de.schlichtherle.truezip.file.TFileOutputStream;

/**
 * API specification for all importers. Subclasses need to implement primarily the parseContentItem method.
 * <p/>
 * The configuration passed as argument should contain all the configuration options that are needed to run
 * the importer. This base class makes use of the following configuration properties:
 * <ul>
 *     <li>lmf.serviceurl - the base url to access the target LMF</li>
 *     <li>lmf.baseuri - the base uri to construct resource URIs for news items (passed to BaseNewsItem.getUri()</li>
 *     <li>lmf.user    - the (optional) user to use for authenticating with the LMF</li>
 *     <li>lmf.pass    - the (optional) password to use for authenticating with the LMF</li>
 *     <li>import.multithread - runs the import multithreading (default: false)</li>
 *     <li>import.delete_before - delete the resource before importing (default: true)</li>     
 *     <li>vocabulary.url - URL from where to access the vocabulary used for looking up predefined concepts</li>
 *     <li>vocabulary.format - RDF format of the vocabulary (ttl or rdf)</li>
 *     <li>geonames.url - URL to the GeoNames webservice to use (default: http://ws.geonames.org/)</li>
 *     <li>geonames.user - (optional) username to authenticate with the GeoNames webservice</li>
 *     <li>geonames.pass - (optional) password to authenticate with the GeoNames webservice</li>
 *     <li>archive.dir     - the directory to use for archiving files</li>
 *     <li>archive.enabled - disable/enable archiving (default: disabled)</li>
 *     <li>archive.delete_on_success - delete the original file when archived (default: false)</li>
 * </ul>
 *
 * @author Sebastian Schaffert
 * @author Sergio Fernández
 */
public abstract class BaseImporter<T extends BaseContentItem> implements Importer<T> {

    protected LMFClientConnector connector;

    protected GeoLookup geoLookup;

    protected Configuration configuration;

    protected VocabularyManager vocabularyManager;

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Create a new importer using the client connector given as argument.
     *
     * @param connector an instance of LMFClientConnector to be used for writing the triples generated by the
     *                  importer to an LMF server
     * @param config    a commons configuration object holding configuration data for the importer; the keys are
     *                  specific to the respective importer
     */
    protected BaseImporter(LMFClientConnector connector, Configuration config) {
        this.connector = connector;
        this.configuration = config;

        checkRequiredConfiguration("lmf.serviceurl");
        checkConditionalConfiguration("archive.enabled", "archive.dir");
        checkOptionalConfiguration("import.multithread", false);
        checkOptionalConfiguration("import.delete_before", true);

    }

    /**
     * Initialise the geolookup service from the configuration. This method can be used by subclasses that need geonames
     * access for URI lookups.
     */
    protected void initialiseGeolookup() {
        this.geoLookup = GeoLookupBuilder.geoLookup(configuration.getString("geonames.url", null))
                .withAccount(configuration.getString("geonames.user"), configuration.getString("geonames.pass"))
                .multiThreadable()
                .create();
    }

    /**
     * Initialise the vocabulary from the configuration. This method can be used by subclasses that need vocabulary
     * access for URI lookups.
     */
    protected void initialiseVocabulary() {
        if(vocabularyManager == null) {
            if(StringUtils.isNotEmpty(configuration.getString("vocabulary.url"))) {
                RDFFormat format;

                String cfgfmt = configuration.getString("vocabulary.format","turtle");

                if("turtle".equals(cfgfmt) || "ttl".equals(cfgfmt)) {
                    format = RDFFormat.TURTLE;
                } else if("xml".equals(cfgfmt) || "rdf".equals(cfgfmt)) {
                    format = RDFFormat.RDFXML;
                } else {
                    log.error("unsupported RDF format for vocabulary: {}", cfgfmt);
                    return;
                }

                try {
                    URL vocabUrl = new URL(configuration.getString("vocabulary.url"));
                    vocabularyManager = new VocabularyManager(vocabUrl.openStream(), format);
                } catch (MalformedURLException e) {
                    log.error("malformed URL for retrieving vocabulary data: {}", e.getMessage());
                } catch (IOException e) {
                    log.error("network error while retrieving vocabulary data: {}", e.getMessage());
                }
            } else {
                InputStream in = this.getClass().getResourceAsStream("/vocabulary.ttl");
                if(in != null) {
                    log.info("using fallback vocabulary found at {}", this.getClass().getResource("/vocabulary.ttl"));
                    vocabularyManager = new VocabularyManager(in, RDFFormat.TURTLE);
                } else {
                    log.error("could not initialise vocabulary, the vocabulary URL is not defined");
                }
            }
        }

    }

    /**
     * Execute the importer, using the configuration passed as arguments of the constructor.
     */
    @Override
    public void runImports() {
    	if (configuration.getBoolean("import.multithread")) {
    		runImportsMultiThread();
    	} else {
    		runImportsMonoThread();
    	}
    }

    /**
     * Execute the importer sequentially: item by item
     */
	protected void runImportsMonoThread() {
		Iterator<T> uris = getContentItems();
        while(uris.hasNext()) {
            try {
				runImport(uris.next());
			} catch (RepositoryException e) {
	            log.error("error accessing in-memory repository", e);
	        }
        }
	}
	
    /**
     * Execute the importer in different threads in parallel
     * @throws InterruptedException 
     */
	protected void runImportsMultiThread() {
        Iterator<T> items = getContentItems();
        
        final ExecutorService executorService = Executors.newFixedThreadPool(5); //TODO: configurable
        ProcessingTracker<T> jobTracker = new ProcessingTracker<T>(executorService);
        while(items.hasNext()){
            jobTracker.register(new ImportJob(items.next()));
        }
        executorService.shutdown();
        try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			
		}
        
        jobTracker.logStatistics();
        log.info(jobTracker.getFailed().size() + " files failed: ");
        for (T failed : jobTracker.getFailed()) {
            log.error(failed.getUri());
        }
	}	

    /**
     * Execute the importer, using the configuration passed as arguments of the constructor.
     * @throws RepositoryException 
     */
	@Override
    public boolean runImport(T item) throws RepositoryException {
        try {
            Repository repository = new SailRepository(new MemoryStore());
            repository.initialize();
            RepositoryConnection connection = repository.getConnection();
            try {
                URI r_newsitem = connection.getValueFactory().createURI(item.getUri());
                URI r_context = (StringUtils.isBlank(getContextUri()) ? null : connection.getValueFactory().createURI(getContextUri()));

                connection.begin();
                parseContentItem(item, connection);
                connection.commit();
                
                connection.begin();
                postProcess(r_newsitem, item, connection, r_context);
                connection.commit();

                updateContentItem(r_newsitem, r_context, connection);
            } finally {
                connection.close();
                repository.shutDown();
            }
        } catch (IOException e) {
            log.error("I/O error while importing data into LMF", e);
            return false;
        }
        return true;
    }


	/**
	 * Hook to add post-processing of the imported/parsed tiples.
	 * @param itemUri the URI of the item that is imported
	 * @param item the T item
	 * @param connection the {@link RepositoryConnection} to manipulate (begin & commit/rollback handled by caller)
	 * @param context the context to import into (may be <code>null</code>)
	 */
    protected void postProcess(URI itemUri, T item,
            RepositoryConnection connection, URI context) {
    }

    /**
     * This method returns the context URI to use for importing triples generated by this importer into the LMF.
     * Subclasses must implement this method and return the appropriate URI, e.g. based on the configuration.
     *
     * @return a string containing the URI of the context to use
     */
    protected abstract String getContextUri();


    /**
     * This method returns an iterator over the URIs of all news items that need to be updated in the current import
     * run. The iterator can e.g. be generated from the items of an RSS feed or from the listing of a fileystem
     * directory. In case the news item URI is generated from some other source (e.g. a filename), the method should
     * implement e.g. a mapping table (with weak keys) to refer back to the original source.
     *
     * @return an iterator over news item descriptors.
     */
    protected abstract Iterator<T> getContentItems();

    /**
     * This method is implemented by subclasses and carries out the transformation from the proprietary format to
     * RDF. The method needs to be implemented appropriately by subclasses. The generated RDF triples should be added
     * to the RepositoryConnection passed as argument. The caller will take care of opening, committing and closing the
     * connection.
     *
     * @param newsItem   a news item descriptor with the information needed for parsing the news item
     * @param connection
     * @throws RepositoryException
     */
    protected abstract void parseContentItem(T newsItem, RepositoryConnection connection) throws RepositoryException;



    /**
     * Update the content item with the URI given as first argument using the data (all triples) provided by the repository
     * connection given as third argument. The update will be carried out in the context (named graph) with the URI given
     * as second argument.
     *
     * @param itemUri     the URI of the content item (news article, wiki article, blog posting, image, video) to update
     * @param contextUri  the context URI where to store the triples in the LMF
     * @param connection  the repository connection containing the updated triples
     * @throws java.io.IOException in case there is an error connecting to the LMF
     * @throws org.openrdf.repository.RepositoryException in case there is an error accessing the triples
     */
    private void updateContentItem(URI itemUri, URI contextUri, RepositoryConnection connection) throws IOException, RepositoryException {
    	if (configuration.getBoolean("import.delete_before")) {
    		connector.deleteContentItem(itemUri);
    	}
    	
        log.info("updating content item {} in LMF ...", itemUri);
        if (log.isTraceEnabled()) {
        	log.trace("raw triples for {}: \n{}\n", itemUri.toString(), exportTriples(connection, RDFFormat.TURTLE));
        }
        
        long start = System.currentTimeMillis();
        connector.updateContentItem(itemUri, contextUri, connection);
        long end   = System.currentTimeMillis();
        log.debug("updated content item in {} ms", end-start);
    }


    /**
     * Check if a required configuration key is present; throws IllegalArgumentException otherwise
     * @param key
     */
    protected void checkRequiredConfiguration(String key) {
        if(configuration.getProperty(key) == null) {
            throw new IllegalArgumentException("the configuration key "+key +" is required but was not found");
        }
    }
    
    /**
     * Check if a required configuration key is present; throws IllegalArgumentException otherwise
     * @param key
     */
    protected void checkOptionalConfiguration(String key, Object value) {
        if(configuration.getProperty(key) == null) {
            configuration.addProperty(key, value);
        }
    }    

    /**
     * Check if a conditional configuration key is present; throws IllegalArgumentException otherwise
     * @param key1 key for a boolean configuration defining the condition
     */
    protected void checkConditionalConfiguration(String key1, String key2) {
        if(configuration.getBoolean(key1,false) && configuration.getProperty(key1) == null) {
            throw new IllegalArgumentException("the configuration key " + key2 +" is required but was not found");
        }
    }


    /**
     * Archive the file passed as first argument in the article archive. The date argument is used to determine the
     * file name of the archive zip file.
     *
     * @param file
     * @param date
     * @return
     */
    protected boolean archiveFile(File file, Date date) {
        if(configuration.getBoolean("archive.enabled", false)) {
            String archiveDir = configuration.getString("archive.dir");
            if (StringUtils.isEmpty(archiveDir)) {
                log.warn("not archiving article as archive directory is not configured");
                return true;
            }

            try {
                File yDir = new File(archiveDir, String.format("%tY", date));
                yDir.mkdirs();

                final File archFile = new TFile(yDir, String.format("%tY-%<tm.zip/%s", date, file.getName()));
                final TFileOutputStream zos = new TFileOutputStream(archFile);

                final FileInputStream fis = new FileInputStream(file);
                byte[] buff = new byte[1024];
                int count;
                do {
                    count = fis.read(buff);
                    if (count > 0) {
                        zos.write(buff, 0, count);
                    }
                } while (count > 0);
                zos.close();
                fis.close();

                if (configuration.getBoolean("archive.delete_on_success", false)) {
                    return file.delete();
                }

                return true;
            } catch (FileNotFoundException e) {
                log.error("Could not open file: {}", e.getLocalizedMessage());
            } catch (IOException e) {
                log.error("I/O error while accessing file", e);
            }
        }
        return false;
    }


    /**
     * Return the base URI used for constructing resource URIs
     * @return the baseUri
     */
    protected String getBaseUri() {
        final String bu = configuration.getString("lmf.baseuri", getServiceUrl()+"resource/");
        if (StringUtils.endsWith(bu, "/")) return bu;
        else return bu+"/";
    }
    
    /**
     * Build the baseUri based on the provided configuration value (lmf.baseuri).
     */
    protected String getServiceUrl() {
        final String serviceUrl = configuration.getString("lmf.serviceurl");
        if(StringUtils.endsWith(serviceUrl,"/")) {
            return serviceUrl;
        } else {
            return serviceUrl + "/";
        }
    }

    private class ImportJob extends ProcessingJob<T> {

        private final T item;

        protected ImportJob(T item) {
            this.item = item;
        }
        
        @Override
        public boolean process() throws IOException {
        	try {
				return runImport(item);
			} catch (RepositoryException e) {
				log.error("Error importing {} ({}): {}", item.getUri(), item.getClass().getName(), e.getMessage());
				return false;
			}
        }

		@Override
		public T getItem() {
			return item;
		}
        
    }    
    
	private String exportTriples(RepositoryConnection conn, RDFFormat format) {
		StringWriter writer = new StringWriter();
		try {
			try {
				RDFWriter w = Rio.createWriter(format, writer);
				conn.export(w);
			} catch (RepositoryException ex) {
				conn.rollback();
			}
		} catch (RDFHandlerException e) {
			log.error("Error writing triples to string", e);
		} catch (RepositoryException ex) {
			log.error("Error accessing repository", ex);
		}
		return writer.toString();
	}
	
}
