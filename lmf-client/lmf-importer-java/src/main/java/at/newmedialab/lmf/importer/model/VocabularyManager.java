/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.model;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.marmotta.commons.vocabulary.SKOS;
import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a simple vocabulary manager with the functionality to lookup the URI of a concept based on its label. It
 * works with SKOS thesauruses and does the lookup on the prefLabel, altLabel, hiddenLabel and altHiddenLabel properties.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class VocabularyManager {

    private static Logger log = LoggerFactory.getLogger(VocabularyManager.class);

    private Repository repository;

    private List<URI> lookupProperties;


    public VocabularyManager(InputStream vocabulary, RDFFormat format) {
        repository = new SailRepository(new MemoryStore());
        try {
            repository.initialize();
            loadData(vocabulary, format);
        } catch (RepositoryException e) {
            log.error("error while initialising vocabulary repository",e);
        }

        lookupProperties = new ArrayList<URI>();
        lookupProperties.add(SKOS.prefLabel);
        lookupProperties.add(SKOS.altLabel);
        lookupProperties.add(SKOS.hiddenLabel);
    }

    /**
     * Get the properties used for concept lookup. Default is skos:prefLabel, skos:altLabel, skos:hiddenLabel
     * @return
     */
    public List<URI> getLookupProperties() {
        return lookupProperties;
    }

    /**
     * Set the properties used for concept lookup. Default is skos:prefLabel, skos:altLabel, skos:hiddenLabel
     */
    public void setLookupProperties(List<URI> lookupProperties) {
        this.lookupProperties = lookupProperties;
    }

    /**
     * Load the vocabulary into the repository.
     * @param vocabulary
     * @param format
     */
    private void loadData(InputStream vocabulary, RDFFormat format) {
        try {
            RepositoryConnection con = repository.getConnection();
            try {
                con.begin();
                con.add(vocabulary, "", format);
                con.commit();
            } catch(RepositoryException ex) {
                con.rollback();
                throw ex;
            } finally {
                con.close();
            }
        } catch (RDFParseException e) {
            log.error("parse error while reading vocabulary file",e);
        } catch (IOException e) {
            log.error("I/O error while reading vocabulary file",e);
        } catch(RepositoryException ex) {
            log.error("could not access in-memory repository",ex);
        }
    }

    /**
     * Do a lookup for the given label and return the first corresponding URI found, if any. Otherwise will log a
     * warning and return null. If there are more than one possible URIs, will reeturn the first and also issue a warning.
     * @param label
     * @return
     */
    public URI lookup(String label, String language) {
        Literal value;
        if(StringUtils.isEmpty(language)) {
            value = repository.getValueFactory().createLiteral(label);
        } else {
            value = repository.getValueFactory().createLiteral(label, language);
        }

        List<URI> results = new ArrayList<URI>();

        try {
            RepositoryConnection con = repository.getConnection();
            try {
                con.begin();

                for(URI prop : lookupProperties) {
                    RepositoryResult<Statement> stmts = con.getStatements(null, prop, value, true);
                    while(stmts.hasNext()) {
                        Statement stmt = stmts.next();
                        if(stmt.getSubject() instanceof URI) {
                            results.add( (URI) stmt.getSubject() );
                        }
                    }
                    stmts.close();
                }

            } finally {
                con.rollback();
                con.close();
            }
        } catch(RepositoryException ex) {
           log.error("error while querying vocabulary",ex);
        }

        if(results.size() == 1) {
            return results.get(0);
        } else if(results.size() > 1) {
            log.info("more than one result for label {}",label);
            return results.get(0);
        } else  {
            log.info("no result found for label {}", label);
            return null;
        }

    }

}
