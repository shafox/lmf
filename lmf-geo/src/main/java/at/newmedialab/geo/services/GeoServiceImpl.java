/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.geo.services;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.apache.marmotta.commons.sesame.model.Namespaces;
import org.apache.marmotta.commons.sesame.repository.ResourceUtils;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.http.HttpClientService;
import org.apache.marmotta.platform.core.events.ConfigurationChangedEvent;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.slf4j.Logger;

import at.newmedialab.geo.api.GeoService;
import at.newmedialab.geo.model.PlaceFacade;
import at.newmedialab.lmf.util.geonames.api.GeoLookup;
import at.newmedialab.lmf.util.geonames.builder.GeoLookupBuilder;


/**
 * @author Thomas Kurz <thomas.kurz@salzburgresearch.at>
 * @author Jakob Frank <jakob.frank@salzburgresearch.at>
 */
@ApplicationScoped
public class GeoServiceImpl implements GeoService {

    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;
    
    @Inject
    private HttpClientService httpClientService;

    private GeoLookup lookupService;

     /* (non-Javadoc)
     * @see at.newmedialab.geo.services.GeoService#resolveLocation(java.lang.String)
     */
    @Override
    public PlaceFacade resolvePlace(final RepositoryConnection connection, final String locationString) {
        PlaceFacade location = null;

        return location;
    }
    
    @Override
    public URI resolveLocation(RepositoryConnection con, String locationString) {
        // Look for the Location in local TS
        final Literal locationStringLit = con.getValueFactory().createLiteral(locationString);
        final String shortcut = Namespaces.NS_GEONAMES + "xLocalShortcut";
        log.debug("Looking for a place called '{}' in local triple store...", locationString);

        try {
            final URI shortcutProp = con.getValueFactory().createURI(shortcut);
            if (locationStringLit != null && shortcutProp != null) {

                RepositoryResult<Statement> candidates = con.getStatements(null,shortcutProp,locationStringLit,true);
                try {
                    if(candidates.hasNext()) {
                        Resource r = candidates.next().getSubject();

                        if (r instanceof URI && ResourceUtils.hasType(con, r, Namespaces.NS_GEONAMES+"P")) {
                            if (log.isDebugEnabled()) {
                                log.debug("Found '{}' in local triple store: <{}>", locationString, r.stringValue());
                            }
                            return (URI) r;
                        }
                    }
                } finally {
                    candidates.close();
                }
            }

            // If local lookup failed - try geonames.org
            URI placeURI = resolveLocation(locationString);
            if (placeURI != null) {
                if (shortcutProp != null && locationStringLit != null) {
                    con.add(placeURI, shortcutProp, locationStringLit);
                }
                return placeURI;
            }
        } catch(RepositoryException ex) {
            log.error("error while accessing RDF repository",ex);
        }
        return null;
    }

    @Override
    public URI resolveLocation(String locationString) {
        log.debug("Looking for a place called '{}' at geonames...", locationString);
        String placeURI = lookupService.resolvePlace(locationString);
        if (placeURI != null) {
            log.debug("Fetched <{}> from geonames", placeURI);
            return new URIImpl(placeURI);
        } else {
            return null;
        }
    }

    @Override
    public PlaceFacade[] resolvePlaces(RepositoryConnection con, String... placeNames) {
        return resolvePlaces(con, Arrays.asList(placeNames)).toArray(new PlaceFacade[0]);
    }
    
    @Override
    public List<PlaceFacade> resolvePlaces(RepositoryConnection con, List<String> placeNames) {
        LinkedList<PlaceFacade> result = new LinkedList<PlaceFacade>();
        for (String string : placeNames) {
            final PlaceFacade resolvedPlace = resolvePlace(con, string);
            if (resolvedPlace != null) {
                result.add(resolvedPlace);
            }
        }
        return result;
    }

    @Override
    public URI[] resolveLocations(RepositoryConnection con, String... placeNames) {
        return resolveLocations(con, Arrays.asList(placeNames)).toArray(new URI[0]);
    }
    
    @Override
    public List<URI> resolveLocations(RepositoryConnection con, List<String> placeNames) {
        LinkedList<URI> result = new LinkedList<URI>();
        for (String string : placeNames) {
            final URI r = resolveLocation(con, string);
            if (r != null) {
                result.add(r);
            }
        }
        return result;
    }

    @Override
    public URI[] resolveLocations(String... placeNames) {
        return resolveLocations(Arrays.asList(placeNames)).toArray(new URI[0]);
    }

    
    @Override
    public List<URI> resolveLocations(List<String> placeNames) {
        LinkedList<URI> result = new LinkedList<URI>();
        for (String string : placeNames) {
            final URI r = resolveLocation(string);
            if (r != null) {
                result.add(r);
            }
        }
        return result;
    }
    
    
    protected void onConfigurationChange(@Observes ConfigurationChangedEvent event) {
        if (event.containsChangedKeyWithPrefix("geonames.")) {
            initialize();
        }
    }

    @PostConstruct
    protected void initialize() {
        final String geoUser = configurationService.getStringConfiguration("geonames.user", null);
        final String geoToken = configurationService.getStringConfiguration("geonames.token", null);
        
        lookupService = GeoLookupBuilder.geoLookup()
            .withAccount(geoUser, geoToken)
            .usingServer(configurationService.getStringConfiguration("geonames.server", null))
            .throughHttpClient(httpClientService.getHttpClient())
            .create();

    }

}
