/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.model.ldpath;

import org.openrdf.model.Resource;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;

import java.util.Set;

/**
 * A class for representing the results of an enhancement processor. Contains the following two fields:
 * <ul>
 *     <li>
 *         a collection of URIs identifying the individual enhancement results; the EnhancerEngineService will connect
 *         the enhanced resource with these URIs using the relation specified in the field name for the enhancement
 *         rule
 *     </li>
 *     <li>
 *         a Sesame repository containing the triples created by the enhancement engine to describe the enhancement
 *         results; it is up to the implementation what kind of repository is used;
 *     </li>
 * </ul>
 * The EnhancementEngineService will call the close() method when it is finished, allowing the result to clean up any
 * resources occupied by the result (e.g. the repository or open connections).
 * <p/>
 * Author: Sebastian Schaffert (sschaffert@apache.org)
 */
public class EnhancementResult {

    /**
     * The resources identifying individual enhancement results, i.e. the entry points to an enhancement structure
     * in the enhancement results repository.
     */
    private Set<Resource> enhancementResources;

    /**
     * The triple store containing the triples created by the enhancement engine to describe the enhancement results.
     */
    private Repository enhancementResults;

    public EnhancementResult(Set<Resource> enhancementResources, Repository enhancementResults) {
        this.enhancementResources = enhancementResources;
        this.enhancementResults = enhancementResults;
    }

    /**
     * The resources identifying individual enhancement results, i.e. the entry points to an enhancement structure
     * in the enhancement results repository.
     */
    public Set<Resource> getEnhancementResources() {
        return enhancementResources;
    }

    /**
     * The triple store containing the triples created by the enhancement engine to describe the enhancement results.
     */
    public Repository getEnhancementResults() {
        return enhancementResults;
    }

    /**
     * Close the enhancement result, freeing any occupied resources and shutting down the enhancement result
     * repository.
     */
    public void close() {
        try {
            enhancementResults.shutDown();
        } catch (RepositoryException e) {
        }
    }
}
