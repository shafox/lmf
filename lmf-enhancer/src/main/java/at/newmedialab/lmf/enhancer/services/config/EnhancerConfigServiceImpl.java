/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.services.config;

import at.newmedialab.lmf.enhancer.api.config.EnhancerConfigService;
import at.newmedialab.lmf.enhancer.exception.EngineAlreadyExistsException;
import at.newmedialab.lmf.enhancer.model.enhancer.EnhancerConfiguration;
import com.google.common.collect.Lists;
import org.apache.marmotta.commons.sesame.filter.SesameFilter;
import org.apache.marmotta.commons.sesame.filter.resource.UriPrefixFilter;
import org.apache.marmotta.ldpath.exception.LDPathParseException;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.events.ConfigurationChangedEvent;
import org.apache.marmotta.platform.core.events.ConfigurationServiceInitEvent;
import org.apache.marmotta.platform.core.model.filter.MarmottaLocalFilter;
import org.apache.marmotta.platform.core.qualifiers.event.Created;
import org.apache.marmotta.platform.core.qualifiers.event.Removed;
import org.apache.marmotta.platform.core.qualifiers.event.Updated;
import org.openrdf.model.Resource;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.*;

/**
 * The enhancer core service provides functionalities for managing the configuration of LMF enhancers.
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class EnhancerConfigServiceImpl implements EnhancerConfigService {

    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;


    @Inject @Created
    private Event<EnhancerConfiguration> engineCreatedEvent;

    @Inject @Updated
    private Event<EnhancerConfiguration> engineUpdatedEvent;

    @Inject @Removed
    private Event<EnhancerConfiguration> engineRemovedEvent;

    private Map<String,EnhancerConfiguration> engines = null;


    /**
     * Don't listen to any configuration changes while storing data
     */
    private boolean storing = false;


    public void startup(@Observes ConfigurationServiceInitEvent event) {
        // trigger initialisation
    }


    @PostConstruct
    public void initialize() {
        log.info("LMF Enhancer Configuration Service initializing (engines: {})", configurationService.getListConfiguration("enhancer.engines"));
        loadEnhancementEngines();
    }

    private void loadEnhancementEngines() {
        if(engines == null) {
            engines = new HashMap<String, EnhancerConfiguration>();

            // load all enhancement engines from the configuration
            for(String engineName : configurationService.getListConfiguration("enhancer.engines")) {
                EnhancerConfiguration engine = loadEnhancementEngine(engineName);
                engines.put(engineName,engine);
            }
        }
    }


    private EnhancerConfiguration loadEnhancementEngine(String name) {
        EnhancerConfiguration engine = new EnhancerConfiguration(name);
        engine.setEnhancementContext(configurationService.getBaseUri()+"enhancer/"+name.toLowerCase());

        reloadEnhancementEngine(name,engine);

        return engine;
    }


    private void reloadEnhancementEngine(String name, EnhancerConfiguration engine) {
        engine.setOptimizeLimit(configurationService.getIntConfiguration("enhancer."+name.toLowerCase()+".optimize",1000));
        engine.setThreads(configurationService.getIntConfiguration("enhancer." + name.toLowerCase() + ".workers", 2));
        engine.setQueueSize(configurationService.getIntConfiguration("enhancer." + name.toLowerCase() + ".queuesize", 100000));

        // initialise filters
        Set<SesameFilter<Resource>> filters = new HashSet<SesameFilter<Resource>>();
        if(configurationService.getBooleanConfiguration("enhancer."+name.toLowerCase()+".local_only",false)) {
            filters.add(MarmottaLocalFilter.getInstance());
        }
        if(configurationService.getListConfiguration("enhancer."+name.toLowerCase()+".accept_prefixes").size() > 0) {
            filters.add(new UriPrefixFilter(new HashSet<String>(configurationService.getListConfiguration("enhancer."+name.toLowerCase()+".accept_prefixes"))));
        }


        engine.setFilters(filters);

        if(!configurationService.getStringConfiguration("enhancer." + name.toLowerCase() + ".program", "").equals(engine.getProgramString())) {
            engine.setProgramString(configurationService.getStringConfiguration("enhancer."+name.toLowerCase()+".program",""));
        }

    }


    private void storeEnhancementEngine(EnhancerConfiguration engine) {
        synchronized (engines) {
            storing = true;

            String prefix = "enhancer." + engine.getName().toLowerCase();

            configurationService.setIntConfiguration(prefix + ".optimize", engine.getOptimizeLimit());
            configurationService.setType(prefix + ".optimize","java.lang.Integer(100|100|*)");
            configurationService.setIntConfiguration(prefix + ".workers", engine.getThreads());
            configurationService.setType(prefix + ".workers","java.lang.Integer(1|1|*)");
            configurationService.setIntConfiguration(prefix + ".queuesize", engine.getQueueSize());
            configurationService.setType(prefix + ".queuesize","java.lang.Integer(1000|1000|*)");


            boolean localOnly = false;
            Set<String> acceptPrefixes = new HashSet<String>();

            for (SesameFilter<Resource> filter : engine.getFilters()) {
                if(filter instanceof MarmottaLocalFilter) {
                    localOnly = true;
                } else if (filter instanceof UriPrefixFilter) {
                    acceptPrefixes = ((UriPrefixFilter) filter).getPrefixes();
                }
            }

            configurationService.setBooleanConfiguration(prefix+".local_only",localOnly);
            configurationService.setType(prefix+".local_only","java.lang.Boolean");

            if(acceptPrefixes.size() > 0) {
                configurationService.setListConfiguration(prefix+".accept_prefixes", new ArrayList<String>(acceptPrefixes));
            } else {
                configurationService.removeConfiguration(prefix+".accept_prefixes");
            }

            configurationService.setConfiguration(prefix+".program",engine.getProgramString());
            configurationService.setType(prefix+".program","org.marmotta.type.Program");

            storing = false;
        }
    }


    /**
     * React to any changes in the configuration that may affect one of the engines
     *
     * @param event
     */
    public void configurationChangedEvent(@Observes ConfigurationChangedEvent event) {
        if(event.containsChangedKeyWithPrefix("enhancer.")) {
            synchronized (engines) {
                if(!storing) {
                    // a configuration option for the enhancer has been changed, check whether we need to update any engine configuration
                    for(Map.Entry<String,EnhancerConfiguration> entry : engines.entrySet()) {
                        for(String key : event.getKeys()) {
                            if(key.startsWith("enhancer."+entry.getKey().toLowerCase())) {
                                reloadEnhancementEngine(entry.getKey(),entry.getValue());

                                engineUpdatedEvent.fire(entry.getValue());
                            }
                        }
                    }
                }
            }

        }
    }

    /**
     * Return a collection of all configured enhancement engines.
     *
     * @return
     */
    @Override
    public List<EnhancerConfiguration> listEnhancementEngines() {
        loadEnhancementEngines();
        synchronized (engines) {
            return Lists.newArrayList(engines.values());
        }
    }

    /**
     * Return true if the enhancement engine with the given name exists.
     *
     * @param name
     * @return
     */
    @Override
    public boolean hasEnhancementEngine(String name) {
        loadEnhancementEngines();
        return engines.containsKey(name);
    }

    /**
     * Return the configuration of the enhancement engine with the given name, or null in case an engine with this
     * name does not exist.
     *
     * @param name
     * @return
     */
    @Override
    public EnhancerConfiguration getEnhancementEngine(String name) {
        loadEnhancementEngines();
        return engines.get(name);
    }


    /**
     * Create and add the enhancement engine with the name and program passed as argument. Throws EngineAlreadyExistsException
     * in case the engine already exists and LDPathParseException in case the program is not correctly parsed.
     *
     * @param name
     * @param program
     * @return the newly created enhancement engine 
     */
    @Override
    public EnhancerConfiguration createEnhancementEngine(String name, String program) throws EngineAlreadyExistsException, LDPathParseException {
        EnhancerConfiguration engine = new EnhancerConfiguration(name);
        engine.setEnhancementContext(configurationService.getBaseUri()+"enhancer/"+name.toLowerCase());

        engine.setThreads(2);
        engine.setOptimizeLimit(1000);
        engine.setProgramString(program);

        Set<SesameFilter<Resource>> filters = new HashSet<SesameFilter<Resource>>();
        filters.add(MarmottaLocalFilter.getInstance());
        engine.setFilters(filters);

        addEnhancementEngine(engine);
        
        return engine;
    }

    /**
     * Add a new enhancement engine to the enhancer configuration. If an engine with this name already exists,
     * an exception is thrown.
     * <p/>
     * Note that this method merely updates the configuration and does not automatically re-run the enhancement
     * process for all resources.
     *
     * @param engine
     */
    @Override
    public void addEnhancementEngine(EnhancerConfiguration engine) throws EngineAlreadyExistsException {
        if(!engines.containsKey(engine.getName())) {
            engines.put(engine.getName(),engine);
            storeEnhancementEngine(engine);

            List<String> enabledEngines = new ArrayList<String>(configurationService.getListConfiguration("enhancer.engines"));
            enabledEngines.add(engine.getName());
            configurationService.setListConfiguration("enhancer.engines",enabledEngines);

            engineCreatedEvent.fire(engine);
        } else
            throw new EngineAlreadyExistsException("the engine with name "+engine.getName()+" already exists");
    }

    /**
     * Update the configuration of the enhancement engine given as argument.
     * <p/>
     * Note that this method merely updates the configuration and does not automatically re-run the enhancement
     * process for all resources.
     *
     * @param engine
     */
    @Override
    public void updateEnhancementEngine(EnhancerConfiguration engine) {
        if(engines.containsKey(engine.getName())) {
            engines.put(engine.getName(),engine);
            storeEnhancementEngine(engine);

            engineUpdatedEvent.fire(engine);
        }
    }

    /**
     * Remove the enhancement engine configuration with the given name.
     * <p/>
     * Note that this method merely updates the configuration and does not automatically re-run the enhancement
     * process for all resources.
     *
     * @param engine
     */
    @Override
    public void removeEnhancementEngine(EnhancerConfiguration engine) {
        if(engines.containsKey(engine.getName())) {
            engines.remove(engine.getName());

            List<String> enabledEngines = new ArrayList<String>(configurationService.getListConfiguration("enhancer.engines"));
            enabledEngines.remove(engine.getName());
            configurationService.setListConfiguration("enhancer.engines",enabledEngines);

            engineRemovedEvent.fire(engine);
        }
    }
}
