/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.classifier.model;

import at.newmedialab.lmf.classifier.exception.ClassificationException;
import at.newmedialab.lmf.classifier.exception.ClassifierNotReadyException;
import com.google.common.base.Preconditions;
import kiwi.core.api.triplestore.ResourceService;
import kiwi.core.model.rdf.KiWiUriResource;
import opennlp.tools.doccat.DoccatModel;
import opennlp.tools.doccat.DocumentCategorizerME;
import opennlp.tools.doccat.DocumentSample;
import opennlp.tools.doccat.DocumentSampleStream;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

/**
 * Representation of an OpenNLP Classifier, identified by a name. Each classifier consists of a name, of a language
 * tag, of a model, and optionally of a training set, and offers functionalities to classify a text and to add
 * training data.
 * <p/>
 * Author: Sebastian Schaffert
 */
public class Classifier {

    private static Logger log = LoggerFactory.getLogger(Classifier.class);


    private String name;

    private int trainingUpdates = 0;

    private File trainingFile;

    private File modelFile;

    private DocumentCategorizerME categorizer;

    private ResourceService resourceService;


    public Classifier(String name, File trainingFile, File modelFile, ResourceService resourceService) {
        this.name = name;
        this.trainingFile = trainingFile;
        this.modelFile = modelFile;
        this.resourceService = resourceService;

        if(modelFile.exists() && modelFile.canRead()) {
            try {
                initCategorizer();
            } catch (ClassificationException e) {}
        }
    }


    /**
     * Initialise the document categorizer using the model file set for the classifier. If the model file does not
     * exist, we try to train the model
     */
    private synchronized void initCategorizer() throws ClassificationException {
        try {
            log.info("Initialising categorizer {} from trained model {} ",name,modelFile.getAbsolutePath());
            InputStream is = FileUtils.openInputStream(modelFile);
            DoccatModel m = new DoccatModel(is);
            categorizer = new DocumentCategorizerME(m);
        } catch (IOException ex) {
            log.error("I/O error while initialising categorizer {} from model file {}; has the model been initialised?",name,modelFile.getAbsolutePath());
            log.error("exception details: ",ex);

            throw new ClassificationException("I/O error while initialising categorizer",ex);
        } catch(Exception ex) {
            log.error("unknown error while initialising categorizer {} from model file {}; has the model been initialised?",name,modelFile.getAbsolutePath());
            log.error("exception details: ",ex);

            throw new ClassificationException("I/O error while initialising categorizer",ex);
        }

    }

    /**
     * Train the model used by the categorizer using the training file. Note that the categorizer will only use the
     * new model after initCategorizer is called.
     * 
     * @throws ClassificationException
     */
    private synchronized void trainModel() throws ClassificationException {
        DoccatModel model;

        try {
            trainingUpdates = 0;
            InputStream dataIn = FileUtils.openInputStream(trainingFile);
            ObjectStream<String> lineStream = new PlainTextByLineStream(dataIn, "UTF-8");
            ObjectStream<DocumentSample> sampleStream = new DocumentSampleStream(lineStream);
            model = DocumentCategorizerME.train("lmf",sampleStream,2,100);

            OutputStream out = FileUtils.openOutputStream(modelFile);
            model.serialize(out);
            out.close();

        } catch (IOException ex) {
            log.error("I/O error while training model from training file {}");
        }

    }


    public String getName() {
        return name;
    }

    public File getTrainingFile() {
        return trainingFile;
    }

    public File getModelFile() {
        return modelFile;
    }

    public int getTrainingUpdates() {
        return trainingUpdates;
    }

    /**
     * Return the categories managed by this classifier.
     * @return
     */
    public Collection<KiWiUriResource> getCategories() throws ClassificationException {
        if(categorizer != null) {
            Set<KiWiUriResource> categories = new HashSet<KiWiUriResource>();
            for(int i=0; i<categorizer.getNumberOfCategories(); i++) {
                KiWiUriResource category = resourceService.getUriResource(categorizer.getCategory(i));
                if(category != null) {
                    categories.add(category);
                }
            }
            return categories;
        } else
            throw new ClassifierNotReadyException("classifier "+name+" is not yet ready");

    }

    /**
     * Classify the text passed as argument using the configured model and return a list of classifications ordered
     * by probability. Each classification associates the KiWiUriResource of a concept with a probability, indicating
     * the likelihood the text matches with the category.
     *
     * @param text             the text to classify
     * @return                 a list of classifications (concept resource and probability) ordered by probability
     */
    public List<Classification> getAllClassifications(String text) throws ClassificationException {
        return getAllClassifications(text, 0.0);
    }



    /**
     * Classify the text passed as argument using the configured model and return a list of classifications ordered
     * by probability. Each classification associates the KiWiUriResource of a concept with a probability, indicating
     * the likelihood the text matches with the category. A minimum probability can be used to return only results
     * with a probability higher than the one given.
     *
     * @param text             the text to classify
     * @param minProbability   the minimum probability
     * @return                 a list of classifications (concept resource and probability) ordered by probability
     */
    public List<Classification> getAllClassifications(String text, double minProbability) throws ClassificationException {
        Preconditions.checkArgument(text != null);
        Preconditions.checkArgument(minProbability >= 0.0);
        Preconditions.checkArgument(minProbability <= 1.0);

        if(categorizer != null) {
            List<Classification> result = new ArrayList<Classification>(categorizer.getNumberOfCategories());

            double[] probabilities = categorizer.categorize(text);
            for(int i = 0; i < probabilities.length; i++) {
                if(probabilities[i] > minProbability) {
                    String category_uri = categorizer.getCategory(i);
                    KiWiUriResource category = resourceService.getUriResource(category_uri);
                    if(category != null) {
                        result.add(new Classification(category,probabilities[i]));
                    } else {
                        log.warn("category {} identified but not found in triple store",category_uri);
                    }
                }
            }
            Collections.sort(result);

            return result;
        } else
            throw new ClassifierNotReadyException("classifier "+name+" is not yet ready");

    }

    /**
     * Return the category with the highest probability for the given text. Simplified version of getAllClassifications.
     * @param text
     * @return
     * @throws ClassificationException
     */
    public KiWiUriResource getBestClassification(String text) throws ClassificationException {
        Preconditions.checkArgument(text != null);

        if(categorizer != null) {
            double[] probabilities = categorizer.categorize(text);
            String category_uri = categorizer.getBestCategory(probabilities);
            return resourceService.getUriResource(category_uri);
        } else
            throw new ClassifierNotReadyException("the classifier "+name+" is not yet ready");
    }


    /**
     * Add an additional sample text as training data for the category passed as first argument. Some simple processing
     * will be applied to the sample text, mainly whitespace normalisation.
     * <p/>
     * Note that adding training data alone will only extend the training file. Retraining needs to be triggered
     * manually before the new training data becomes effective in the classifier.
     * 
     * @param category    the category for which to add training data
     * @param sampleText  a sample text to use as training data
     */
    public synchronized void addTrainingData(KiWiUriResource category, String sampleText) {
        Preconditions.checkArgument(category != null);
        Preconditions.checkArgument(sampleText != null);
        Preconditions.checkArgument(!"".equals(sampleText.trim()));

        synchronized (trainingFile) {

            if(!"".equals(sampleText.trim())) {
                try {
                    OutputStream os = FileUtils.openOutputStream(trainingFile,true);
                    PrintWriter out = new PrintWriter(new OutputStreamWriter(os));

                    String content = sampleText.replaceAll("\\s+", " ").trim();
                    out.println(category.getUri() + " " + content);

                    trainingUpdates++;

                    out.close();
                } catch (IOException e) {
                    log.error("I/O error; could not add training data to training file {}",trainingFile.getAbsolutePath());
                    log.error("exception details",e);
                }
            }
        }
    }


    /**
     * Clear all training data associated with this classifier. Useful if training went into the wrong paths :-)
     */
    public void clearTrainingData() {
        synchronized (trainingFile) {
            if(trainingFile.canWrite()) {
                trainingFile.delete();
                trainingUpdates = 0;
            }
        }
    }


    /**
     * Retrain the system using the training file. Will create a new classification model and then reinitialise the
     * classifier.
     *
     * @throws ClassificationException when training failed
     */
    public void retrain() throws ClassificationException {
        log.info("retraining classifier {} ...",name);
        trainModel();
        initCategorizer();
    }

}
